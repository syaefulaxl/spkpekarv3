-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 04 Okt 2018 pada 13.12
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_spkpekar`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_department`
--

CREATE TABLE `tb_department` (
  `dept_id` int(1) NOT NULL,
  `nama_dept` varchar(30) DEFAULT NULL,
  `kepala_dept` varchar(50) DEFAULT NULL,
  `status` enum('Aktif','Tidak Aktif') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_department`
--

INSERT INTO `tb_department` (`dept_id`, `nama_dept`, `kepala_dept`, `status`) VALUES
(1, 'Sales', 'Boobalan', 'Aktif'),
(2, 'Shipping', 'Goengoen', 'Aktif'),
(3, 'Service', 'Yanyan', 'Aktif'),
(4, 'IT', 'Karthick', 'Aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_karyawan`
--

CREATE TABLE `tb_karyawan` (
  `emp` varchar(9) NOT NULL,
  `nama_kary` varchar(30) DEFAULT NULL,
  `posisi` varchar(30) DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `kelamin_kary` enum('L','P') DEFAULT NULL,
  `email_kary` varchar(30) DEFAULT NULL,
  `telp_kary` varchar(13) DEFAULT NULL,
  `alamat_kary` text,
  `tempat_lahir_kary` varchar(30) DEFAULT NULL,
  `tanggal_lahir_kary` date DEFAULT NULL,
  `agama_kary` varchar(7) DEFAULT NULL,
  `status` enum('Aktif','Tidak Aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_karyawan`
--

INSERT INTO `tb_karyawan` (`emp`, `nama_kary`, `posisi`, `dept_id`, `kelamin_kary`, `email_kary`, `telp_kary`, `alamat_kary`, `tempat_lahir_kary`, `tanggal_lahir_kary`, `agama_kary`, `status`) VALUES
('EMP070', 'BIta', 'Service', 3, 'P', 'bita@texcoms.com', NULL, NULL, NULL, '1987-11-11', NULL, 'Aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kriteria`
--

CREATE TABLE `tb_kriteria` (
  `kode_kriter` varchar(7) NOT NULL,
  `nama_kriter` varchar(50) DEFAULT NULL,
  `atribut` enum('Benefit','Cost') NOT NULL,
  `bobot` tinyint(2) NOT NULL,
  `deskripsi` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kriteria`
--

INSERT INTO `tb_kriteria` (`kode_kriter`, `nama_kriter`, `atribut`, `bobot`, `deskripsi`) VALUES
('ABSEN', 'Kehadiran', 'Benefit', 10, ''),
('K1', 'Target Kerja', 'Benefit', 20, 'Hasil Kerja Sesuai Target Yang Ditentukan'),
('K2', 'Kedisiplinan', 'Benefit', 20, ''),
('K3', 'Tanggung Jawab', 'Benefit', 15, ''),
('K4', 'Kesalahan Kerja', 'Cost', 25, ''),
('K5', 'Wasting Time', 'Cost', 10, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_nilai`
--

CREATE TABLE `tb_nilai` (
  `nilai_id` int(11) NOT NULL,
  `emp` varchar(9) DEFAULT NULL,
  `tanggal_nilai` date DEFAULT NULL,
  `absen` tinyint(2) DEFAULT NULL,
  `total` decimal(5,2) DEFAULT NULL,
  `deskripsi` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_nilai_detail`
--

CREATE TABLE `tb_nilai_detail` (
  `nilai_detail_id` int(11) NOT NULL,
  `nilai_id` int(11) NOT NULL,
  `kode_kriter` varchar(7) NOT NULL,
  `nilai` tinyint(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_users` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status_login` enum('Admin','Direktur','Manajer') NOT NULL,
  `status` enum('Aktif','Tidak Aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_users`, `username`, `password`, `nama`, `email`, `status_login`, `status`) VALUES
(1, 'Backdoor', '0a67ce5c676f792d1098158bfed7a5f2cca826e4', 'Backdoor', 'backdoor@gmail.com', 'Admin', 'Aktif'),
(2, 'Syaeful', '41abd58e149457c76eaa02f0bce2651eccf552a0', 'Syaeful', 'it.bdg@texcoms.com', 'Admin', 'Aktif'),
(3, 'Adam', 'f941e1206abd4a2d8889da67be10151f429d95dc', 'Adam', 'adam@texcoms.com', 'Admin', 'Aktif'),
(4, 'Harirao', '0af062477014cca836eb10b86c65f55414c6e41c', 'Harirao', 'harirao@texcoms.com', 'Manajer', 'Aktif'),
(5, 'Boobalan', '4075a202066e4ccc45be6eb6f82181693689442d', 'Boobalan', 'boobalan@texcoms.com', 'Direktur', 'Aktif');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_department`
--
ALTER TABLE `tb_department`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indeks untuk tabel `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  ADD PRIMARY KEY (`emp`),
  ADD KEY `FK_tb_karyawan` (`dept_id`);

--
-- Indeks untuk tabel `tb_kriteria`
--
ALTER TABLE `tb_kriteria`
  ADD PRIMARY KEY (`kode_kriter`);

--
-- Indeks untuk tabel `tb_nilai`
--
ALTER TABLE `tb_nilai`
  ADD PRIMARY KEY (`nilai_id`),
  ADD KEY `FK_tb_nilai` (`emp`);

--
-- Indeks untuk tabel `tb_nilai_detail`
--
ALTER TABLE `tb_nilai_detail`
  ADD PRIMARY KEY (`nilai_detail_id`),
  ADD KEY `FK_tb_nilai_kriter` (`kode_kriter`),
  ADD KEY `FK_tb_nilai_detail` (`nilai_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_users`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_department`
--
ALTER TABLE `tb_department`
  MODIFY `dept_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_nilai`
--
ALTER TABLE `tb_nilai`
  MODIFY `nilai_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_nilai_detail`
--
ALTER TABLE `tb_nilai_detail`
  MODIFY `nilai_detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  ADD CONSTRAINT `FK_tb_karyawan` FOREIGN KEY (`dept_id`) REFERENCES `tb_department` (`dept_id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_nilai`
--
ALTER TABLE `tb_nilai`
  ADD CONSTRAINT `FK_tb_nilai` FOREIGN KEY (`emp`) REFERENCES `tb_karyawan` (`emp`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_nilai_detail`
--
ALTER TABLE `tb_nilai_detail`
  ADD CONSTRAINT `FK_tb_nilai_detail` FOREIGN KEY (`nilai_id`) REFERENCES `tb_nilai` (`nilai_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_tb_nilai_kriter` FOREIGN KEY (`kode_kriter`) REFERENCES `tb_kriteria` (`kode_kriter`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
