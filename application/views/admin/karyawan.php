<script type="text/javascript">
function edit_karyawan($emp,$name,$email,$posisi,$dept_id,$kelamin,$tanggal) {
     var emp              = $emp;
     var nama_kary         = $name;
     var email_kary        = $email;
     var posisi          = $posisi;
     var dept_id         = $dept_id;
     var kelamin_kary      = $kelamin;
     var tanggal_lahir_kary= $tanggal;

     $(".modal2 #emp").val( emp );
     $(".modal2 #nama_kary").val( nama_kary );
     $(".modal2 #email_kary").val( email_kary );
     $(".modal2 #posisi").val( posisi );
     $(".modal2 #dept_id").val( dept_id );
     $(".modal2 #kelamin_kary").val( kelamin_kary );
     $(".modal2 #tanggal_lahir_kary").val( tanggal_lahir_kary );
     $("#update_karyawan").modal()

}
</script>
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Daftar karyawan</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class="row">
                                <div class="col-lg-12" align="right">
                                <a href="#" data-toggle="modal" data-target="#import_K" class="btn btn-social btn-success"><i class="fa fa-download"></i>Import Data Karyawan</a>
                                <a href="#" data-toggle="modal" data-target="#add_karyawan" class="btn btn-social btn-primary"><i class="fa fa-plus"></i>Tambahkan Karyawan</a>
                                </div>
                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>EMP</th>
                                        <th>Nama Karyawan</th>
                                        <th>Posisi</th>
                                        <th>Department</th>
                                        <th>E-Mail</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 0;
                                        foreach ($data_karyawan as $key) {
                                        $no++;
                                        
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $key->emp; ?></td>
                                        <td><?php echo $key->nama_kary; ?></td>
                                        <td><?php echo $key->posisi; ?></td>
                                        <td><?php echo $key->nama_dept;  ?></td>
                                        <td><?php echo $key->email_kary;  ?></td>
                                        <td><?php echo $key->tanggal_lahir_kary;  ?></td>
                                        <td><?php echo $key->status;  ?></td>
                                        <td align="center">
                                            <a href="javascript:edit_karyawan('<?php echo $key->emp; ?>','<?php echo $key->nama_kary; ?>','<?php echo $key->email_kary; ?>','<?php echo $key->posisi; ?>')"><i class="-square fa fa-pencil-square fa-fw fa-lg text-warning"></i></a>
                                            <a href="<?php echo site_url('karyawan/update_status') ?>?emp=<?php echo $key->emp; ?>" onclick="return confirm('Apakah anda yakin akan mengnonaktifkan / mengaktifkan karyawan ini?') "><i class="-square fa fa-fw fa-lg fa-exchange text-danger"></i></a>
                                            <a href="<?php echo site_url('karyawan/delete') ?>?emp=<?php echo $key->emp; ?>" onclick="return confirm('Apakah anda yakin akan menghapus karyawan ini?') "><i class="-square fa fa-fw fa-lg fa-minus-square text-danger"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                             <p><i>
                            * <i class="-square fa fa-pencil-square fa-fw fa-lg text-warning"></i> untuk mengubah data karyawan. <br>
                            * <i class="-square fa fa-fw fa-lg fa-exchange text-danger"></i> untuk menonaktifkan dan mengaktifkan karyawan. <br>
                            * <i class="-square fa fa-fw fa-lg fa-minus-square text-danger"></i> untuk menghapus data karyawan.
                            </i></p>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
<!-- TAMBAH -------------------------------------------------------------------------------------------------->
                        <div class="modal fade col-md-12" id="add_karyawan" role="dialog" aria-labelledby="title_plus" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="title_plus">Tambahkan Karyawan</h4>
                                    </div>
                                    <div class="modal-body">
                                                <form action="<?php echo site_url('karyawan/add') ?>" method="post" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label>EMP</label>
                                                        <input class="form-control" type="text" name="emp" placeholder="EMP" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nama Karyawan</label>
                                                        <input class="form-control" type="text" name="nama_kary" placeholder="Nama karyawan" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Posisi</label>
                                                        <input class="form-control" type="text" name="posisi" placeholder="Posisi" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Department</label>
                                                        <select class="form-control" name="dept_id" placeholder="posisi Studi" required >
                                                            <option value="">- Pilih Status Akses -</option>
                                                            <option value="1">Sales</option>
                                                            <option value="2">Shipping</option>
                                                            <option value="3">Services</option>
                                                            <option value="4">IT</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Kelamin karyawan</label>
                                                        <select class="form-control" name="kelamin_kary" placeholder="Kelamin karyawan" required >
                                                            <option value="">- Pilih Status Akses -</option>
                                                            <option value="L">Laki - Laki</option>
                                                            <option value="P">Perempuan</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>E-Mail</label>
                                                        <input class="form-control" type="email" name="email_kary" placeholder="E-Mail karyawan" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Tanggal Lahir karyawan</label>
                                                        <input class="form-control" type="date" name="tanggal_lahir_kary" placeholder="Tanggal Lahir karyawan" required >
                                                        </input>
                                                    </div>
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>
<!-- UPDATE -------------------------------------------------------------------------------------------------->
                        <div class="modal fade col-md-12" id="update_karyawan" role="dialog" aria-labelledby="title_plus" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="title_plus">Edit karyawan</h4>
                                    </div>
                                    <div class="modal-body modal2">
                                                <form action="<?php echo site_url('karyawan/update') ?>" method="post" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label>EMP</label>
                                                        <input class="form-control" type="text" name="eemp" id="emp" placeholder="emp" required readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nama Karyawan</label>
                                                        <input class="form-control" type="text" name="enama_kary" id="nama_kary" placeholder="Nama karyawan" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Posisi</label>
                                                        <input class="form-control" type="text" name="eposisi" id="posisi" placeholder="Posisi" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Department</label>
                                                        <select class="form-control" name="edept_id" id="dept_id" placeholder="Department" required >
                                                            <option value="">- Pilih Status Akses -</option>
                                                            <option value="1">Sales</option>
                                                            <option value="2">Shipping</option>
                                                            <option value="3">Services</option>
                                                            <option value="4">IT</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Kelamin karyawan</label>
                                                        <select class="form-control" name="ekelamin_kary" id="kelamin_kary" placeholder="Kelamin karyawan" required >
                                                            <option value="">- Pilih Status Akses -</option>
                                                            <option value="L">Laki - Laki</option>
                                                            <option value="P">Perempuan</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>E-Mail</label>
                                                        <input class="form-control" type="email" name="eemail_kary" id="email_kary" placeholder="E-Mail karyawan" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Tanggal Lahir karyawan</label>
                                                        <input class="form-control" type="date" name="etanggal_lahir_kary" id="tanggal" placeholder="Tanggal Lahir karyawan" required >
                                                        </input>
                                                    </div> 
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-warning">Update</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>
<!-- IMPORT -------------------------------------------------------------------------------------------------->
                        <div class="modal fade col-md-12" id="import_K" role="dialog" aria-labelledby="title_plus" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="title_plus">Import Data Karyawan</h4>
                                    </div>
                                    <div class="modal-body">
                                                <form action="<?php echo site_url('karyawan/import_emp') ?>" method="post" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label>Pilih Data Karyawan</label>
                                                        <input class="form-control" type="file" name="5file" required>
                                                        <input type="hidden" name="iemp" value="<?php echo @$data_karyawan[0]->emp; ?>" required>
                                                    </div>
                                                    <div class="form-group" align="center">
                                                    <a href="<?php echo base_url(); ?>sources/doc/format-karyawan.xlsx" class="btn btn-social btn-success"><i class="fa fa-download"></i>Download Format Excel</a>
                                                    </div>
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success">Import Data</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>
