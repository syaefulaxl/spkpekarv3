<script type="text/javascript">
function edit_users($username,$name,$email) {
     var username     = $username;
     var nama         = $name;
     var email        = $email;

     $(".modal2 #username").val( username );
     $(".modal2 #nama").val( nama );
     $(".modal2 #email").val( email );
     $("#update_users").modal()

}
</script>
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Daftar Pengguna</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class="row">
                                <div class="col-lg-12" align="right">
                                <a href="#" data-toggle="modal" data-target="#add_users" class="btn btn-social btn-primary"><i class="fa fa-plus"></i>Tambahkan Pengguna</a>
                                </div>
                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Username</th>
                                        <th>Nama</th>
                                        <th>E-mail</th>
                                        <th>Status</th>
                                        <th>Akses</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 0;
                                        foreach ($data_users as $key) {
                                        $no++;
                                        
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $key->username; ?></td>
                                        <td><?php echo $key->nama; ?></td>
                                        <td><?php echo $key->email; ?></td>
                                        <td><?php echo $key->status_login; ?></td>
                                        <td><?php echo $key->status; ?></td>
                                        <td align="center">
                                            <a href="javascript:edit_users('<?php echo $key->username; ?>','<?php echo $key->nama; ?>','<?php echo $key->email; ?>','<?php echo $key->username; ?>')"><i class="-square fa fa-pencil-square fa-fw fa-lg text-warning"></i></a>
                                            <a href="<?php echo site_url('users/update_status') ?>?username=<?php echo $key->username; ?>" onclick="return confirm('Apakah anda yakin akan mengnonaktifkan / mengaktifkan pengguna ini?') "><i class="-square fa fa-fw fa-lg fa-exchange text-danger"></i></a>
                                            <a href="<?php echo site_url('users/delete') ?>?username=<?php echo $key->username; ?>" onclick="return confirm('Apakah anda yakin akan menghapus pengguna ini?') "><i class="-square fa fa-fw fa-lg fa-minus-square text-danger"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                             <p><i>
                            * <i class="-square fa fa-pencil-square fa-fw fa-lg text-warning"></i> untuk mengubah data pengguna. <br>
                            * <i class="-square fa fa-fw fa-lg fa-exchange text-danger"></i> untuk menonaktifkan dan mengaktifkan pengguna. <br>
                            * <i class="-square fa fa-fw fa-lg fa-minus-square text-danger"></i> untuk menghapus data pengguna.
                            </i></p>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
                        <div class="modal fade col-md-12" id="add_users" role="dialog" aria-labelledby="title_plus" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="title_plus">Tambahkan Pengguna</h4>
                                    </div>
                                    <div class="modal-body">
                                                <form action="<?php echo site_url('users/add') ?>" method="post" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label>Nama</label>
                                                        <input class="form-control" type="text" name="nama" placeholder="Nama" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>E-mail</label>
                                                        <input class="form-control" type="email" name="email" placeholder="E-mail" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Username</label>
                                                        <input class="form-control" type="text" name="username" placeholder="Username" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Password</label>
                                                        <input class="form-control" type="password" name="password" placeholder="Password" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Status Hak Akses</label>
                                                        <select class="form-control" name="status_login" required >
                                                            <option value="0">- Pilih Status Akses -</option>
                                                            <option value="Admin">Admin</option>
                                                            <option value="Direktur">Direktur</option>
                                                            <option value="Manajer">Manajer</option>
                                                        </select>
                                                    </div>
                                                
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade col-md-12" id="update_users" role="dialog" aria-labelledby="title_plus" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="title_plus">Edit Pengguna</h4>
                                    </div>
                                    <div class="modal-body modal2">
                                                <form action="<?php echo site_url('users/update') ?>" method="post" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label>Nama</label>
                                                        <input class="form-control" type="text" name="enama" id="nama" placeholder="Nama" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>E-mail</label>
                                                        <input class="form-control" type="email" name="eemail" id="email" placeholder="E-mail" required readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Username</label>
                                                        <input class="form-control" type="text" name="eusername" id="username" placeholder="Username" required readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Password</label>
                                                        <input class="form-control" type="password" name="epassword" placeholder="Password" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Status Hak Akses</label>
                                                        <select class="form-control" name="estatus_login" required >
                                                            <option value="0">- Pilih Status Akses -</option>
                                                            <option value="Admin">Admin</option>
                                                            <option value="Direktur">Direktur</option>
                                                            <option value="Manajer">Manajer</option>
                                                        </select>
                                                    </div>
                                                
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-warning">Update</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>