<script type="text/javascript">
function edit_department($dept_id,$name,$email) {
     var dept_id             = $dept_id;
     var nama_dept         = $name;
     var kepala_dept        = $email;

     $(".modal2 #dept_id").val( dept_id );
     $(".modal2 #nama_dept").val( nama_dept );
     $(".modal2 #kepala_dept").val( kepala_dept );
     $("#update_department").modal()

}
</script>
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Daftar Department</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class="row">
                                <div class="col-lg-12" align="right">
                                <a href="#" data-toggle="modal" data-target="#add_department" class="btn btn-social btn-primary"><i class="fa fa-plus"></i>Tambahkan department</a>
                                </div>
                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>ID Department</th>
                                        <th>Nama Department</th>
                                        <th>Kepala Department</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 0;
                                        foreach ($data_department as $key) {
                                        $no++;
                                        
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $key->dept_id; ?></td>
                                        <td><?php echo $key->nama_dept; ?></td>
                                        <td><?php echo $key->kepala_dept; ?></td>
                                        <td><?php echo $key->status;  ?></td>
                                        <td align="center">
                                            <a href="javascript:edit_department('<?php echo $key->dept_id; ?>','<?php echo $key->nama_dept; ?>','<?php echo $key->kepala_dept; ?>')"><i class="-square fa fa-pencil-square fa-fw fa-lg text-warning"></i></a>
                                            <a href="<?php echo site_url('department/update_status') ?>?dept_id=<?php echo $key->dept_id; ?>" onclick="return confirm('Apakah anda yakin akan mengnonaktifkan / mengaktifkan department ini?') "><i class="-square fa fa-fw fa-lg fa-exchange text-danger"></i></a>
                                            <a href="<?php echo site_url('department/delete') ?>?dept_id=<?php echo $key->dept_id; ?>" onclick="return confirm('Apakah anda yakin akan menghapus department ini?') "><i class="-square fa fa-fw fa-lg fa-minus-square text-danger"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                             <p><i>
                            * <i class="-square fa fa-pencil-square fa-fw fa-lg text-warning"></i> untuk mengubah data department. <br>
                            * <i class="-square fa fa-fw fa-lg fa-exchange text-danger"></i> untuk menonaktifkan dan mengaktifkan department. <br>
                            * <i class="-square fa fa-fw fa-lg fa-minus-square text-danger"></i> untuk menghapus data department.
                            </i></p>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
                        <div class="modal fade col-md-12" id="add_department" role="dialog" aria-labelledby="title_plus" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="title_plus">Tambahkan department</h4>
                                    </div>
                                    <div class="modal-body">
                                                <form action="<?php echo site_url('department/add') ?>" method="post" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label>dept_id</label>
                                                        <input class="form-control" type="text" name="dept_id" placeholder="dept_id" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nama department</label>
                                                        <input class="form-control" type="text" name="nama_dept" placeholder="Nama department" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email department</label>
                                                        <input class="form-control" type="email" name="kepala_dept" placeholder="Email" required >
                                                    </div>
                                                    <div class="form-group">
                                                        <label>No Telepon</label>
                                                        <input class="form-control" type="text" name="telp_dsn" placeholder=" No Telepon" required="true" data-inputmask="&quot;mask&quot;: &quot;9999-9999-9999&quot;" data-mask="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Alamat Rumah</label>
                                                        <input class="form-control" type="text" name="alamat_dsn" placeholder="Alamat Rumah">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Tanggal Lahir department</label>
                                                        <input class="form-control" type="date" name="tanggal_lahir_dsn" placeholder="Tanggal Lahir department" required >
                                                        </input>
                                                    </div>
                                                
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade col-md-12" id="update_department" role="dialog" aria-labelledby="title_plus" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="title_plus">Edit department</h4>
                                    </div>
                                    <div class="modal-body modal2">
                                                <form action="<?php echo site_url('department/update') ?>" method="post" enctype="multipart/form-data">
                                                   <div class="form-group">
                                                        <label>dept_id</label>
                                                        <input class="form-control" type="text" name="edept_id" id="dept_id" placeholder="dept_id" required readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nama department</label>
                                                        <input class="form-control" type="text" name="enama_dept" id="nama_dept" placeholder="Nama department" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Program Kelas</label>
                                                        <select class="form-control" name="eprogram" id="program" placeholder="Program Kelas" required >
                                                            <option value="">- Pilih Status Akses -</option>
                                                            <option value="Pagi">Pagi / Reguler</option>
                                                            <option value="Sore">Sore / Non-Reguler</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Semester</label>
                                                        <select class="form-control" name="esemester" id="semester" placeholder="Semester" required >
                                                            <option value="">- Pilih Semester -</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Program Studi</label>
                                                        <select class="form-control" name="eprodi_id" id="prodi_id" placeholder="Program Studi" required >
                                                            <option value="">- Pilih Status Akses -</option>
                                                            <option value="1">Teknik Informatika</option>
                                                            <option value="2">Manajemen Informatika</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Kelamin department</label>
                                                        <select class="form-control" name="ekelamin_dsn" id="kelamin_dsn" placeholder="Kelamin department" required >
                                                            <option value="">- Pilih Status Akses -</option>
                                                            <option value="L">Laki - Laki</option>
                                                            <option value="P">Perempuan</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>E-Mail</label>
                                                        <input class="form-control" type="email" name="ekepala_dept" id="kepala_dept" placeholder="E-Mail department" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Tanggal Lahir department</label>
                                                        <input class="form-control" type="date" name="etanggal_lahir_dsn" id="tanggal" placeholder="Tanggal Lahir department" required >
                                                        </input>
                                                    </div>
                                                
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-warning">Update</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>