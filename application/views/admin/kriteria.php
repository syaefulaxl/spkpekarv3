<script type="text/javascript">
function edit_kriteria($kode_kriter,$nama_kriter,$atribut,$bobot,$deskripsi) {
     var kode_kriter      = $kode_kriter;
     var nama_kriter      = $nama_kriter;
     var atribut      = $atribut;
     var bobot      = $bobot;
     var deskripsi        = $deskripsi;

     if(kode_kriter == "ABSEN"){
      document.getElementById("div_atribut").style.display = "none";
     } else {
      document.getElementById("div_atribut").style.display = "block";   
     }

     $(".modal2 #kode_kriter").val( kode_kriter );
     $(".modal2 #nama_kriter").val( nama_kriter );
     $(".modal2 #atribut").val( atribut );
     $(".modal2 #bobot").val( bobot );
     $(".modal2 #deskripsi").val( deskripsi );
     $("#update_kriteria").modal()

}
</script>
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Daftar kriteria</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!--<div class="panel-heading">
                        <div class="row">
                                <div class="col-lg-12" align="right">
                                <a href="#" data-toggle="modal" data-target="#add_kriteria" class="btn btn-social btn-primary"><i class="fa fa-plus"></i>Tambahkan kriteria</a>
                                </div>
                        </div>
                        </div>-->
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Kriteria</th>
                                        <th>Nama kriteria</th>
                                        <th>Atribut</th>
                                        <th>Bobot</th>
                                        <th>Deskripsi</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 0;
                                        foreach ($data_kriteria as $key) {
                                        $no++;
                                        
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $key->kode_kriter; ?></td>
                                        <td><?php echo $key->nama_kriter; ?></td>
                                        <td><?php echo $key->atribut; ?></td>
                                        <td><?php echo $key->bobot; ?></td>
                                        <td><?php echo $key->deskripsi; ?></td>
                                        <td align="center">
                                            <a href="javascript:edit_kriteria('<?php echo $key->kode_kriter; ?>','<?php echo $key->nama_kriter; ?>','<?php echo $key->atribut; ?>','<?php echo $key->bobot; ?>','<?php echo $key->deskripsi; ?>')"><i class="-square fa fa-pencil-square fa-fw fa-lg text-warning"></i></a>
                                            <!-- <a href="<?php echo site_url('kriteria/delete') ?>?kode_kriter=<?php echo $key->kode_kriter; ?>" onclick="return confirm('Apakah anda yakin akan menghapus kriteria ini?') "><i class="-square fa fa-fw fa-lg fa-minus-square text-danger"></i></a> -->
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                             <p><i>
                            * <i class="-square fa fa-pencil-square fa-fw fa-lg text-warning"></i> untuk mengubah data kriteria. <br>
                            <!-- * <i class="-square fa fa-fw fa-lg fa-minus-square text-danger"></i> untuk menghapus data kriteria. -->
                            </i></p>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
                        <div class="modal fade col-md-12" id="add_kriteria" role="dialog" aria-labelledby="title_plus" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="title_plus">Tambahkan Kriteria</h4>
                                    </div>
                                    <div class="modal-body">
                                                <form action="<?php echo site_url('kriteria/add') ?>" method="post" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label>Kode kriteria</label>
                                                        <input class="form-control" type="text" name="kode_kriter" placeholder="Kode Kriteria" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nama kriteria</label>
                                                        <input class="form-control" type="text" name="nama_kriter" placeholder="Nama Kriteria" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Deskripsi</label>
                                                        <input class="form-control" type="text" name="deskripsi" placeholder="Deskripsi Kriteria" >
                                                    </div>
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade col-md-12" id="update_kriteria" role="dialog" aria-labelledby="title_plus" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="title_plus">Edit kriteria</h4>
                                    </div>
                                    <div class="modal-body modal2">
                                                <form action="<?php echo site_url('kriteria/update') ?>" method="post" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label>Kode kriteria</label>
                                                        <input class="form-control" type="text" name="ekode_kriter" id="kode_kriter" placeholder="Kode Kriteria" required readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nama kriteria</label>
                                                        <input class="form-control" type="text" name="enama_kriter" id="nama_kriter" placeholder="Nama Kriteria" required>
                                                    </div>  
                                                    <div class="form-group" id="div_atribut">
                                                        <label>Atribut</label>
                                                        <select class="form-control" name="eatribut" placeholder="Atribut">
                                                            <option value="">- Pilih Atribut -</option>
                                                            <option value="benefit">Benefit</option>
                                                            <option value="cost">Cost</option>
                                                        </select>
                                                    </div>  
                                                    <div class="form-group">
                                                        <label>Bobot</label>
                                                        <input class="form-control" type="text" name="ebobot" id="bobot" placeholder="Bobot" required>
                                                    </div>        
                                                    <div class="form-group">
                                                        <label>Deskripsi</label>
                                                        <input class="form-control" type="text" name="edeskripsi" id="deskripsi" placeholder="Deskripsi Kriteria" >
                                                    </div>
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-warning">Update</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>