
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Data Anda</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class="row">
                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                                
                                 <form action="<?php echo site_url('profile/update_akun') ?>" method="post" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label>Username</label>
                                                        <input class="form-control" type="text" name="username" value="<?php echo $data_users[0]->username ?>" required readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nama</label>
                                                        <input class="form-control" type="text" name="nama" value="<?php echo $data_users[0]->nama ?>" placeholder="Nama" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>E-mail</label>
                                                        <input class="form-control" type="email" name="email" value="<?php echo $data_users[0]->email ?>" placeholder="E-mail" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Password Baru</label>
                                                        <input class="form-control" type="password" name="baru" placeholder="Password Baru" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Password Lama</label>
                                                        <input class="form-control" type="password" name="lama" placeholder="Password Lama" required>
                                                    </div>
                                                    <div class="form-group" align="right">         
                                                        <button type="submit" class="btn btn-warning">Update</button>
                                                    </div>
                                            </form>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
             			