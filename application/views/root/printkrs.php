<script type="text/javascript">
        function zoom() {
            document.body.style.zoom = "80%"
        }
        function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
        }
</script>
<style onload="zoom()"></style>
<div class="row">
                <div class="col-lg-12">
                    <br>
                    
                    <a href="#" onclick="printDiv('printableArea')" class="btn btn-social btn-success" style="float: right;"><i class="fa fa-print"></i>Print</a>

                    <br>
                    
                    <h1 class="page-header">Kartu Rencana Studi</h1>
                    
                </div>
                <!-- /.col-lg-12 -->
</div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default" id="printableArea">
                        <div class="panel-heading">
                        <div class="row">
                                <div class="col-lg-12" align="right">
                                        <table width="100%">
                                            <tr>
                                                <td width="20%" style="font-weight: bold;">Jurusan</td>
                                                <td width="3%" style="font-weight: bold;">:</td>
                                                <td style="font-weight: bold;"><?php echo @$data_krs[0]->nama_prodi?></td>
                                            </tr>
                                            <tr>
                                                <td width="20%" style="font-weight: bold;">Nama Mahasiswa</td>
                                                <td width="3%" style="font-weight: bold;">:</td>
                                                <td style="font-weight: bold;"><?php echo @$data_krs[0]->nama_mhs; ?></td>
                                            </tr>
                                            <tr>
                                                <td width="20%" style="font-weight: bold;">Nomor Induk Mahasiswa</td>
                                                <td width="3%" style="font-weight: bold;">:</td>
                                                <td style="font-weight: bold;"><?php echo @$data_krs[0]->nim; ?></td>
                                            </tr>
                                            <tr>
                                                <td width="20%" style="font-weight: bold;">Semester</td>
                                                <td width="3%" style="font-weight: bold;">:</td>
                                                <td style="font-weight: bold;"><?php echo @$data_krs[0]->semester; ?></td>
                                            </tr>
                                            <tr>
                                                <td width="20%" style="font-weight: bold;">Tanggal Penyerahan KRS</td>
                                                <td width="3%" style="font-weight: bold;">:</td>
                                                <td style="font-weight: bold;"><?php echo @$data_krs[0]->tgl_penyerahan; ?></td>
                                            </tr>
                                            <tr>
                                                <td width="20%" style="font-weight: bold;">Tanggal Persetujuan KRS</td>
                                                <td width="3%" style="font-weight: bold;">:</td>
                                                <td style="font-weight: bold;"><?php echo @$data_krs[0]->tgl_persetujuan; ?></td>
                                            </tr>
                                        </table>
                                    
                                </div>
                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <h2 style="font-weight: bold;">Daftar Matakuliah</h2>
                            <hr>
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode</th>
                                        <th>Matakuliah</th>
                                        <th>SKS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 0;
                                        foreach ($data_krs as $key) {
                                        $no++;
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $key->kode_mk; ?></td>
                                        <td><?php echo $key->nama_mk; ?></td>
                                        <td><?php echo $key->sks; ?></td>   
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <p><i> * Silahkan Cetak Untuk Bukti Fisik                        
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->