<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<!--================================================================================
    Item Name: AXEL - CODEBASE
    Version: 1.0.0
    Author: Axelone - syaefulaxl@gmail.com
================================================================================ -->

<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Author: Axelone - syaefulaxl@gmail.com">
  <meta name="keywords" content="Perwalian">

  <title><?php echo $this->config->item('site_title') ?></title>

  <!-- Favicons-->
  <link rel="icon" href="<?php echo base_url()?>uploads/base-img/favicon/favicon-32x32.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url() ?>uploads/base-img/favicon/apple-touch-icon-152x152.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="<?php echo base_url() ?>uploads/base-img/favicon/mstile-144x144.png">
  <!-- For Windows Phone -->
  
  <!-- CORE CSS -->

  <!-- Auth Style CSS -->
  <link href="<?php echo base_url();?>sources/css/auth.css" type="text/css" rel="stylesheet" media="screen,projection">
  
  <!-- Plugins Style CSS -->
  <link href="<?php echo base_url();?>sources/css/normalize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  
</head>

<body>
    <form class="form" id="form" action="<?php echo base_url(); ?>auth/login" method="post" enctype="multipart/form-data">      
      <div class="field email">
        <div class="icon"></div>
        <input class="input" placeholder="Username" id="username" name="username" type="text" autofocus required>
      </div>
      <div class="field password">
        <div class="icon"></div>
        <input class="input" placeholder="Password" id="password" name="password" type="password" required>
      </div>
      <button type="submit" class="button" id="submit">LOGIN
        <div class="side-top-bottom"></div>
        <div class="side-left-right"></div>
      </button><small><a href="<?php echo site_url('auth/forgot_password') ?>">Lupa password ?</a></small>
    </form>

    <!-- Jquery Core -->
    <script type="text/javascript" src="<?php echo base_url();?>sources/js/jquery-3.2.1.min.js"></script>    

    <!-- Auth java -->
    <script type="text/javascript" src="<?php echo base_url();?>sources/js/auth.js"></script>

</body>

</html>
