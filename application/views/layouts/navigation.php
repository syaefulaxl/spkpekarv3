    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#" style="color: white;">Apps nilai</a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php if($status_login == "Admin" || $status_login == "Manajer"){ ?>
                        <li><a href="<?php echo site_url('profile') ?>"><i class="fa fa-user fa-fw"></i> Profile Anda</a>
                        </li>
                        <?php } ?>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url('auth/logout') ?>"><i class="fa fa-sign-out fa-fw"></i> Keluar</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <br>
                            <center>
                                <div style="color: white;">Selamat Datang</div>
                                <div style="color: white;">Hak akses anda : <strong><?php echo $status_login; ?></strong></div>
                                <h3 style="color: white;"><?php echo $name; ?></h3>
                            </center>
                            <br>
                        </li>
                        <?php if($status_login == "Admin" || $status_login == "Manajer" | $status_login == "Direktur"){ ?>
                        <li>
                            <a href="<?php echo site_url('dashboard') ?>"><i class="glyphicon glyphicon-home fa-fw"></i> Beranda</a>
                        </li>
                        <?php
                            }
                        if($status_login == "Admin" || $status_login == "Manajer" | $status_login == "Direktur"){ ?>
                        <li>
                            <a href="<?php echo site_url('nilai') ?>"><i class="glyphicon glyphicon-th-list fa-fw"></i> Daftar Nilai</a>
                        </li>
                        <?php
                            }
                        if($status_login == "Admin"){ ?>
                       <li>
                            <a href="#"><i class="fa fa-database fa-fw"></i> Data Induk<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="glyphicon fa fa-users" href="<?php echo site_url('karyawan') ?>"> Karyawan</a>
                                </li>
                                <li>
                                    <a class="glyphicon glyphicon-th-list" href="<?php echo site_url('kriteria') ?>"> Kriteria</a>
                                </li>
                                <li>
                                    <a class="glyphicon glyphicon-education" href="<?php echo site_url('department') ?>"> Department</a>
                                </li>
                            </ul>
                        </li>
                        <?php
                            }
                        if($status_login == "Admin"){ ?>
                        <li>
                            <a href="<?php echo site_url('users') ?>"><i class="fa fa-users fa-fw"></i> Data Pengguna</a>
                        </li>
                        <?php 
                            } 
                            if($status_login == "Admin" || $status_login == "Manajer" || $status_login == "Direktur"){ ?>
                        <li>
                            <a href="#"><i class="fa fa-database fa-fw"></i> Laporan<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="glyphicon fa fa-users" href="<?php echo site_url('lap_nilai') ?>"> Nilai</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
