        <div class="">
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
              <b>Version</b> <?php echo $this->config->item('version') ?>
            </div>
            <strong>Copyright &copy; <?php echo $this->config->item('site_years') ?> <a class="link" href="<?php echo $this->config->item('site_google') ?>"><?php echo $this->config->item('site_copyright') ?></a>.</strong> All rights
            reserved.
          </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

   <!-- jQuery -->
    <script src="<?php echo base_url(); ?>sources/themes/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>sources/themes/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>sources/themes/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <!-- <script src="<?php echo base_url(); ?>sources/themes/vendor/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>sources/themes/vendor/morrisjs/morris.min.js"></script>
    <script src="<?php echo base_url(); ?>sources/themes/data/morris-data.js"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>sources/themes/dist/js/sb-admin-2.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>sources/themes/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>sources/themes/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>sources/themes/vendor/datatables-responsive/dataTables.responsive.js"></script>
    
    <!-- Select2 JavaScript -->
    <script src="<?php echo base_url(); ?>sources/select2/select2/dist/js/select2.min.js"></script>

    <!-- Custom DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>sources/custom/js/custom-select2.js"></script>
    <script src="<?php echo base_url(); ?>sources/custom/js/custom-datatables.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script src="<?php echo base_url(); ?>sources/themes/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>sources/custom/js/custom-back.js"></script>


</body>

</html>