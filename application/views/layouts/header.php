<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>

<!--================================================================================
    Item Name: AXEL - CODEBASE
    Version: 1.0.0
    Author: Axelone - syaefulaxl@gmail.com
================================================================================ -->

  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Author: Axelone - syaefulaxl@gmail.com">
  <meta name="keywords" content="Perwalian">

  <title><?php echo $this->config->item('site_title') ?></title>

      <link rel='shortcut icon' href='<?php echo base_url(); ?>sources/img/ico.png' type='image/x-icon'>
    <link rel='icon' href='<?php echo base_url(); ?>sources/img/ico.png' type='image/x-icon'>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>sources/themes/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Social Buttons CSS -->
    <link href="<?php echo base_url(); ?>sources/themes/vendor/bootstrap-social/bootstrap-social.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url(); ?>sources/themes/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>sources/themes/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url(); ?>sources/themes/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="<?php echo base_url(); ?>sources/themes/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_url(); ?>sources/themes/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>sources/themes/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">

    <link href="<?php echo base_url(); ?>sources/select2/select2/dist/css/select2.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>sources/themes/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"  rel="stylesheet">

    <link href="<?php echo base_url(); ?>sources/css/AdminLTE.css"  rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<script type="text/javascript">
        function zoom() {
            document.body.style.zoom = "90%"
        }
</script>
<style onload="zoom()"></style>	