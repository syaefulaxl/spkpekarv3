<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Laporan Nilai</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class="row">
                                <div class="col-lg-12" align="right">
                                    <a href="<?php echo $generate_url; ?>" class="btn btn-social btn-warning"><i class="fa fa-cogs"></i>Hitung Nilai</a>
                                    <a href="<?php echo $export_url; ?>" class="btn btn-social btn-success"><i class="fa fa-file-o"></i>Export Nilai</a>
                                </div>

                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>EMP</th>
                                        <th>Nama</th>
                                        <th>Tanggal Nilai</th>
                                        <th>K1</th>
                                        <th>K2</th>
                                        <th>K3</th>
                                        <th>K4</th>
                                        <th>K5</th>
                                        <th>Absen</th>
                                        <th>Total</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 0;
                                        foreach ($data_nilai as $key) {
                                        $no++;
                                        
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $key->emp; ?></td>
                                        <td><?php echo $key->nama_kary; ?></td>
                                        <td><?php 
                                            if ($key->tgl_nilai == "0000-00-00"){
                                                echo "-"; 
                                            } else {
                                                echo date("d/m/Y", strtotime($key->tgl_nilai)); 
                                            }
                                        ?></td>
                                        <td align="right"><?php echo number_format($key->k1,0); ?></td>
                                        <td align="right"><?php echo number_format($key->k2,0); ?></td>
                                        <td align="right"><?php echo number_format($key->k3,0); ?></td>
                                        <td align="right"><?php echo number_format($key->k4,0); ?></td>
                                        <td align="right"><?php echo number_format($key->k5,0); ?></td>
                                        <td align="right"><?php echo number_format($key->absen,0); ?></td>
                                        <td align="right"><?php echo number_format($key->Total,2); ?></td>
                                        <td><?php echo $key->deskripsi; ?></td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->