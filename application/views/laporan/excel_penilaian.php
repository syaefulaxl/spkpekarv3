<?php 
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Penilaian.xls");
?>
<html>
<body> 
<h3><strong>Laporan Penilaian</strong></h3>

                            <table width="1200px" border="1px" style="border: 1px solid;">
                                    <tr bgcolor="orange">
                                        <td width="10px" align="center"><b>No</b></td>
                                        <td width="100px" align="center"><b>EMP</b></td>
                                        <td width="300px" align="center"><b>Nama</td></td>
                                        <td width="100px" align="center"><b>Tanggal Penilaian</b></td>
                                        <td width="40px" align="center"><b>K1</b></td>
                                        <td width="40px" align="center"><b>K2</b></td>
                                        <td width="40px" align="center"><b>K3</b></td>
                                        <td width="40px" align="center"><b>K4</b></td>
                                        <td width="40px" align="center"><b>K5</b></td>
                                        <td width="40px" align="center"><b>Absen</b></td>
                                        <td width="100px" align="center"><b>Total</b></td>
                                        <td width="350px" align="center"><b>Keterangan</b></td>
                                    </tr>
                                    <?php
                                        $no = 0;
                                        foreach ($data_penilaian as $key) {
                                        $no++;
                                        
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $key->emp; ?></td>
                                        <td><?php echo $key->nama_kary; ?></td>
                                        <td align="center"><?php 
                                            if ($key->tgl_nilai == "0000-00-00"){
                                                echo "-"; 
                                            } else {
                                                echo date("d, M Y", strtotime($key->tgl_nilai)); 
                                            }
                                        ?></td>
                                        <td align="right"><?php echo number_format($key->k1,0); ?></td>
                                        <td align="right"><?php echo number_format($key->k2,0); ?></td>
                                        <td align="right"><?php echo number_format($key->k3,0); ?></td>
                                        <td align="right"><?php echo number_format($key->k4,0); ?></td>
                                        <td align="right"><?php echo number_format($key->k5,0); ?></td>
                                        <td align="right"><?php echo number_format($key->absen,0); ?></td>
                                        <td align="right"><?php echo number_format($key->total,2); ?></td>
                                        <td><?php echo $key->deskripsi; ?></td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                            </table>
</body>
</html>