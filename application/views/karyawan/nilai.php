<script type="text/javascript">
function edit_nilai($nilai_id,$emp,$nama_kary,$tgl_nilai,$k1,$k2,$k3,$k4,$k5,$absen) {
     var nilai_id            = $nilai_id;
     var emp                 = $emp;
     var nama_kary           = $nama_kary;
     var tgl_nilai           = $tgl_nilai;
     var k1                  = $k1;
     var k2                  = $k2;
     var k3                  = $k3;
     var k4                  = $k4;
     var k5                  = $k5;
     var absen               = $absen;

     $(".modal2 #nilai_id").val( nilai_id );
     $(".modal2 #emp").val( emp );
     $(".modal2 #nama_kary").val( nama_kary );
     $(".modal2 #tgl_nilai").val( tgl_nilai );
     $(".modal2 #k1").val( k1 );
     $(".modal2 #k2").val( k2 );
     $(".modal2 #k3").val( k3 );
     $(".modal2 #k4").val( k4 );
     $(".modal2 #k5").val( k5 );
     $(".modal2 #absen").val( absen );

     $("#update_nilai").modal()

}

function get_emp(){
        var nama_kary = $("#nama_kary0").val();
        //console.log(nama_kary);
        $.ajax({
            type: "POST",
            url: "nilai/get_data_emp",
            data: "nama_kary0="+nama_kary,
            success: function(html) {

                var myobj = JSON.parse(html);

                if(myobj.valid_emp == true){
                    document.getElementById("emp0").value = myobj.emp_value;
                } else {
                    document.getElementById("emp0").value = "";
                }

                //console.log(myobj.emp_value);
            }
        });
    }

</script>

<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Daftar nilai</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class="row">
                                <div class="col-lg-12" align="right">
                                <!-- <a href="#" data-toggle="modal" data-target="#import_S" class="btn btn-social btn-success"><i class="fa fa-download"></i>Import Data nilai</a> -->
                                <a href="#" data-toggle="modal" data-target="#add_nilai" class="btn btn-social btn-primary"><i class="fa fa-plus"></i>Tambahkan nilai</a>
                                </div>

                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>EMP</th>
                                        <th>Nama</th>
                                        <th>Tanggal nilai</th>
                                        <th>K1</th>
                                        <th>K2</th>
                                        <th>K3</th>
                                        <th>K4</th>
                                        <th>K5</th>
                                        <th>Absen</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 0;
                                        foreach ($data_nilai as $key) {
                                        $no++;
                                        
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $key->emp; ?></td>
                                        <td><?php echo $key->nama_kary; ?></td>
                                        <td><?php 
                                            if ($key->tgl_nilai == "0000-00-00"){
                                                echo "-"; 
                                            } else {
                                                echo $key->tgl_nilai; 
                                            }
                                        ?></td>
                                        <td><?php echo $key->k1; ?></td>
                                        <td><?php echo $key->k2; ?></td>
                                        <td><?php echo $key->k3; ?></td>
                                        <td><?php echo $key->k4; ?></td>
                                        <td><?php echo $key->k5; ?></td>
                                        <td><?php echo $key->absen; ?></td>
                                        <td align="center">
                                            <a href="javascript:edit_nilai('<?php echo $key->nilai_id; ?>','<?php echo $key->emp; ?>','<?php echo $key->nama_kary; ?>','<?php echo $key->tgl_nilai; ?>','<?php echo $key->k1; ?>','<?php echo $key->k2; ?>','<?php echo $key->k3; ?>','<?php echo $key->k4; ?>','<?php echo $key->k5; ?>','<?php echo $key->absen; ?>')"><i class="-square fa fa-pencil-square fa-fw fa-lg text-warning"></i></a>
                                            <a href="<?php echo site_url('nilai/delete_nilai') ?>?nilai_id=<?php echo $key->nilai_id; ?>" onclick="return confirm('Apakah anda yakin akan menghapus nilai ini?') "><i class="-square fa fa-fw fa-lg fa-minus-square text-danger"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                             <p><i>
                            * <i class="-square fa fa-info fa-fw fa-lg text-info"></i> untuk mengisi data nilai. <br>
                                <i>
                            * <i class="-square fa fa-pencil-square fa-fw fa-lg text-warning"></i> untuk mengubah data nilai. <br>
                            * <i class="-square fa fa-fw fa-lg fa-minus-square text-danger"></i> untuk menghapus data nilai.
                            </i></p>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

<!-- TAMBAH -------------------------------------------------------------------------------------------------->
                        <div class="modal fade col-md-12" id="add_nilai" role="dialog" aria-labelledby="title_plus" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="title_plus">Tambahkan nilai</h4>
                                    </div>
                                    <div class="modal-body">
                                                <form action="<?php echo site_url('nilai/add_nilai') ?>" method="post" enctype="multipart/form-data">
                                                    <?php
                                                        $username = $this->session->userdata('username');
                                                    ?>
                                                    <div class="form-group">
                                                        <label>EMP</label>
                                                        <input class="form-control" type="text" name="emp" id="emp0" placeholder="EMP" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nama Karyawan</label>
                                                        <select class="form-control select2 " name="nama_kary" id="nama_kary0" onchange="get_emp();" style="width: 100%;">
                                                        <option value="0">- Pilih Nama Karyawan -</option>
                                                        <?php
                                                            foreach ($data_nilai_2 as $key) {
                                                            ?>
                                                            <option value="<?php echo $key->nama_kary; ?>"><?php echo $key->nama_kary; ?></option>
                                                        <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Tanggal nilai</label>
                                                        <input class="form-control" type="date" value="<?php echo date("Y-m-d");?>" name="tgl_nilai" placeholder="Tanggal nilai" required readonly>
                                                        </input>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>K1</label>
                                                        <input class="form-control" type="text" name="k1" placeholder="Nilai K1" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>K2</label>
                                                        <input class="form-control" type="text" name="k2" placeholder="Nilai K2" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>K3</label>
                                                        <input class="form-control" type="text" name="k3" placeholder="Nilai K3" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>K4</label>
                                                        <input class="form-control" type="text" name="k4" placeholder="Nilai K4" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>K5</label>
                                                        <input class="form-control" type="text" name="k5" placeholder="Nilai K5" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Absen</label>
                                                        <input class="form-control" type="number" step="1" min="0" max="22" name="absen" placeholder="Nilai Absen" required>
                                                    </div>
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>
<!-- UPDATE -------------------------------------------------------------------------------------------------->
                        <div class="modal fade col-md-12" id="update_nilai" role="dialog" aria-labelledby="title_plus" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="title_plus">Edit nilai</h4>
                                    </div>
                                    <div class="modal-body modal2">
                                                <form action="<?php echo site_url('nilai/update_nilai') ?>" method="post" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                    <?php
                                                        $username = $this->session->userdata('username');
                                                    ?>
                                                    <div class="form-group">
                                                        <label>EMP</label>
                                                        <input class="form-control" type="text" name="eemp" id="emp" placeholder="EMP" required readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nama Karyawan</label>
                                                        <input class="form-control" type="text" name="enama_kary" id="nama_kary" placeholder="EMP" required readonly>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Tanggal nilai</label>
                                                        <input class="form-control" type="date" value="<?php echo date("Y-m-d");?>" name="etgl_nilai" id="tgl_nilai" placeholder="Tanggal nilai" required readonly>
                                                        </input>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>K1</label>
                                                        <input class="form-control" type="text" name="ek1" id="k1" placeholder="Nilai K1" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>K2</label>
                                                        <input class="form-control" type="text" name="ek2" id="k2" placeholder="Nilai K2" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>K3</label>
                                                        <input class="form-control" type="text" name="ek3" id="k3" placeholder="Nilai K3" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>K4</label>
                                                        <input class="form-control" type="text" name="ek4" id="k4"placeholder="Nilai K4" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>K5</label>
                                                        <input class="form-control" type="text" name="ek5" id="k5" placeholder="Nilai K5" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Absen</label>
                                                        <input class="form-control" type="text" name="eabsen" id="absen" placeholder="Nilai Absen" required>
                                                    </div>
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-warning">Update</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>
<!-- IMPORT -------------------------------------------------------------------------------------------------->
                        <div class="modal fade col-md-12" id="import_S" role="dialog" aria-labelledby="title_plus" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="title_plus">Import Data Struktur Pekerjaan</h4>
                                    </div>
                                    <div class="modal-body">
                                                <form action="<?php echo site_url('work_activities/import_structural') ?>" method="post" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label>Kode Kegiatan | Nama Kegiatan</label>
                                                        <select class="form-control select2 " name="6id_kegiatan" style="width: 100%;">
                                                        <option value="0">- Kode Kegiatan / Nama Kegiatan -</option>
                                                        <?php
                                                            foreach ($data_activities as $key2) {
                                    
                                                            ?>
                                                            <option value="<?php echo $key2->id_kegiatan; ?>"><?php echo $key2->kode_kegiatan; ?> || <?php echo $key2->nama; ?></option>
                                                        <?php } ?>
                                                        </select>
                                                        <input type="hidden" name="6kode_proyek" value="<?php echo @$data_project[0]->kode_proyek; ?>" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Pilih Data Struktur Pekerjaan</label>
                                                        <input class="form-control" type="file" name="6file" required>
                                                    </div>
                                                    <div class="form-group" align="center">
                                                    <a href="<?php echo base_url(); ?>assets/doc/format-struktur-kerja.xlsx" class="btn btn-social btn-success"><i class="fa fa-download"></i>Download Format Excel</a>
                                                    </div>

                                                
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success">Import Data</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>
