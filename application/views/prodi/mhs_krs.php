<script type="text/javascript">
function edit_krs($krs_id) {
     var krs_id                 = $krs_id;

     $(".modal2 #krs_id").val( krs_id );

     $("#update_krs").modal()

}
</script>
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Daftar KRS</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class="row">
                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Mahasiswa</th>
                                        <th>NIM</th>
                                        <th>Semester</th>
                                        <th>Tanggal Penyerahan</th>
                                        <th>Tanggal Persetujuan</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 0;
                                        foreach ($data_krs as $key) {
                                        $no++;
                                        
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $key->nama_mhs; ?></td>
                                        <td><?php echo $key->nim; ?></td>
                                        <td><?php echo $key->semester; ?></td>
                                        <td><?php echo $key->tgl_penyerahan; ?></td>
                                        <td><?php echo $key->tgl_persetujuan; ?></td>
                                        <td><?php echo $key->status;  ?></td>
                                        <?php
                                            if($key->status == "Belum Disetujui"){
                                        ?>
                                            <td align="center">
                                            <a href="<?php echo site_url('krs/mhs_krs_list') ?>?krs_id=<?php echo $key->krs_id; ?>&status=<?php echo $key->status; ?>"><i class="-square fa fa-fw fa-lg fa-info text-info"></i></a>
                                        <?php
                                            }
                                            if($key->status == "Tidak Disetujui"){
                                        ?>
                                            <td align="center">
                                            <a href="<?php echo site_url('krs/mhs_krs_list') ?>?krs_id=<?php echo $key->krs_id; ?>&status=<?php echo $key->status; ?>"><i class="-square fa fa-fw fa-lg fa-info text-info"></i></a>
                                        <?php
                                            }
                                            if($key->status == "Disetujui"){
                                        ?>
                                            <td align="center">
                                            <a href="<?php echo site_url('krs/mhs_krs_list') ?>?krs_id=<?php echo $key->krs_id; ?>&status=<?php echo $key->status; ?>"><i class="-square fa fa-fw fa-lg fa-info text-info"></i></a>
                                            <a href="<?php echo site_url('cetak') ?>?krs_id=<?php echo $key->krs_id; ?>&status=<?php echo $key->status; ?>"><i class="-square fa fa-fw fa-lg fa-print text-success"></i></a>
                                        <?php
                                            }
                                        ?>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                             <p><i>
                            * <i class="-square fa fa-info fa-fw fa-lg text-info"></i> untuk mengisi data krs / matakuliah yg dipilih. <br>
                                <i>
                            * <i class="-square fa fa-pencil-square fa-fw fa-lg text-warning"></i> untuk mengubah data krs. <br>
                            * <i class="-square fa fa-fw fa-lg fa-minus-square text-danger"></i> untuk menghapus data krs.
                            </i></p>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
                        <div class="modal fade col-md-12" id="add_krs" role="dialog" aria-labelledby="title_plus" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="title_plus">Tambahkan krs</h4>
                                    </div>
                                    <div class="modal-body">
                                                <form action="<?php echo site_url('krs/add_krs') ?>" method="post" enctype="multipart/form-data">
                                                    <?php
                                                        $username = $this->session->userdata('username');
                                                    ?>
                                                    <div class="form-group">
                                                        <label>NIM</label>
                                                        <input class="form-control" type="text" readonly="" value="<?php echo $username; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Semester</label>
                                                        <select class="form-control" name="semester" placeholder="Semester" required >
                                                            <option value="">- Pilih Semester -</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                        </select>
                                                    </div>
                                                    
                                                
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade col-md-12" id="update_krs" role="dialog" aria-labelledby="title_plus" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="title_plus">Edit krs</h4>
                                    </div>
                                    <div class="modal-body modal2">
                                                <form action="<?php echo site_url('krs/update_krs') ?>" method="post" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                    <?php
                                                        $username = $this->session->userdata('username');
                                                    ?>
                                                        <label>NIM</label>
                                                        <input class="form-control" type="text" readonly="" value="<?php echo $username; ?>">
                                                        <input class="form-control" type="hidden" name="ekrs_id" id="krs_id" readonly="" value="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Semester</label>
                                                        <select class="form-control" name="esemester" id="esemester" placeholder="Semester" required >
                                                            <option value="">- Pilih Semester -</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                        </select>
                                                    </div>
                                                
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-warning">Update</button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>