<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Daftar Matakuliah</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>
            <div class="row">
            <div class="panel">
                        <div class="row">
                                <div class="col-lg-12" align="right">
                                <?php 
                                    if ($status == "Belum Disetujui"){
                                        $status_form = "";
                                ?>
                                <form action="<?php echo site_url('krs/results') ?>" method="post" enctype="multipart/form-data">
                                <button type="submit" name="statusx" value="Disetujui" class="btn btn-social btn-success"><i class="fa fa-pencil"></i> Disetujui</button>
                                <button type="submit" name="statusx" value="Tidak Disetujui" class="btn btn-social btn-danger"><i class="fa fa-pencil"></i> Tidak Disetujui</button>    
                                <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>"> <button type="button" class="btn btn-social btn-primary"><i class="fa fa-pencil"></i> Kembali</button></a>
                                <?php 
                                    } elseif ($status == "Tidak Disetujui"){
                                        echo "Dokumen Status : <label>".$status."</label>";
                                        $status_form = "disabled";
                                        ?>
                                        <form action="<?php echo site_url('krs/results') ?>" method="post" enctype="multipart/form-data">
                                        <button type="submit" name="statusx" value="Disetujui" class="btn btn-social btn-success"><i class="fa fa-pencil"></i> Disetujui</button>
                                        <?php
                                
                                    } else {
                                        echo "Dokumen Status : <label>".$status."</label>";
                                        $status_form = "disabled";
                                        ?>
                                        <form action="<?php echo site_url('krs/results') ?>" method="post" enctype="multipart/form-data">
                                        <button type="submit" name="statusx" value="Tidak Disetujui" class="btn btn-social btn-danger"><i class="fa fa-pencil"></i> Tidak Disetujui</button>
                                        <?php
                                    }
                                ?>
                                <input class="form-control" type="hidden" name="krs_id" required="" value="<?php echo $krs_id; ?>">
                                <input class="form-control" type="hidden" name="status" required="" value="<?php echo $status; ?>">

                                </div>
                        </div>
            </div>
            <?php 
                $data_mk = array();
                foreach ($data_perwalian as $value) {
                    array_push($data_mk , $value->kode_mk);
                }

            ?>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class="row">
                                <div class="col-lg-12" align="Left">
                                <p style="font-size: 20px;">
                                    Semester 1</p>
                                </div>
                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>√</th>
                                        <th>Nama Matakuliah</th>
                                        <th>SKS</th>
                                        <th>Prasyarat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($data_matakuliah as $key) {
                                        if($key->semester==1){

                                            if(in_array($key->kode_mk, $data_mk)){
                                                $checked = "checked";
                                            } else {
                                                $checked = "";
                                            }                         
                                    ?>
                                    <tr>
                                        <td><input type="checkbox" disabled <?php echo $checked; ?> name="kode_mk[]" value="<?php echo $key->kode_mk; ?>" <?php echo $status_form; ?>></td>
                                        <td><?php echo $key->nama_mk; ?></td>
                                        <td><?php echo $key->sks; ?></td>
                                        <td><?php echo $key->prasyarat_mk; ?></td>
                                    </tr>
                                    <?php
                                        }}
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class="row">
                                <div class="col-lg-12" align="Left">
                                <p style="font-size: 20px;">
                                    Semester 2</p>
                                </div>
                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>√</th>
                                        <th>Nama Matakuliah</th>
                                        <th>SKS</th>
                                        <th>Prasyarat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($data_matakuliah as $key) {
                                        if($key->semester==2){          

                                            if(in_array($key->kode_mk, $data_mk)){
                                                $checked = "checked";
                                            } else {
                                                $checked = "";
                                            }                             
                                    ?>  
                                    <tr>
                                        <td><input type="checkbox" disabled <?php echo $checked; ?> name="kode_mk[]" value="<?php echo $key->kode_mk; ?>"  <?php echo $status_form; ?>></td>
                                        <td><?php echo $key->nama_mk; ?></td>
                                        <td><?php echo $key->sks; ?></td>
                                        <td><?php echo $key->prasyarat_mk; ?></td>
                                    </tr>
                                    <?php
                                        }}
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class="row">
                                <div class="col-lg-12" align="Left">
                                <p style="font-size: 20px;">
                                    Semester 3</p>
                                </div>
                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>√</th>
                                        <th>Nama Matakuliah</th>
                                        <th>SKS</th>
                                        <th>Prasyarat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($data_matakuliah as $key) {
                                        if($key->semester==3){    

                                            if(in_array($key->kode_mk, $data_mk)){
                                                $checked = "checked";
                                            } else {
                                                $checked = "";
                                            }                                       
                                    ?>
                                    <tr>
                                        <td><input type="checkbox" disabled <?php echo $checked; ?> name="kode_mk[]" value="<?php echo $key->kode_mk; ?>"  <?php echo $status_form; ?>></td>
                                        <td><?php echo $key->nama_mk; ?></td>
                                        <td><?php echo $key->sks; ?></td>
                                        <td><?php echo $key->prasyarat_mk; ?></td>
                                    </tr>
                                    <?php
                                        }}
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class="row">
                                <div class="col-lg-12" align="Left">
                                <p style="font-size: 20px;">
                                    Semester 4</p>
                                </div>
                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>√</th>
                                        <th>Nama Matakuliah</th>
                                        <th>SKS</th>
                                        <th>Prasyarat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($data_matakuliah as $key) {
                                        if($key->semester==4){    

                                            if(in_array($key->kode_mk, $data_mk)){
                                                $checked = "checked";
                                            } else {
                                                $checked = "";
                                            }                                       
                                    ?>
                                    <tr>
                                        <td><input type="checkbox" disabled <?php echo $checked; ?> name="kode_mk[]" value="<?php echo $key->kode_mk; ?>"  <?php echo $status_form; ?>></td>
                                        <td><?php echo $key->nama_mk; ?></td>
                                        <td><?php echo $key->sks; ?></td>
                                        <td><?php echo $key->prasyarat_mk; ?></td>
                                    </tr>
                                    <?php
                                        }}
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class="row">
                                <div class="col-lg-12" align="Left">
                                <p style="font-size: 20px;">
                                    Semester 5</p>
                                </div>
                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>√</th>
                                        <th>Nama Matakuliah</th>
                                        <th>SKS</th>
                                        <th>Prasyarat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($data_matakuliah as $key) {
                                        if($key->semester==5){ 

                                            if(in_array($key->kode_mk, $data_mk)){
                                                $checked = "checked";
                                            } else {
                                                $checked = "";
                                            }                                          
                                    ?>
                                    <tr>
                                        <td><input type="checkbox" disabled <?php echo $checked; ?> name="kode_mk[]" value="<?php echo $key->kode_mk; ?>"  <?php echo $status_form; ?>></td>
                                        <td><?php echo $key->nama_mk; ?></td>
                                        <td><?php echo $key->sks; ?></td>
                                        <td><?php echo $key->prasyarat_mk; ?></td>
                                    </tr>
                                    <?php
                                        }}
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class="row">
                                <div class="col-lg-12" align="Left">
                                <p style="font-size: 20px;">
                                    Semester 6</p>
                                </div>
                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>√</th>
                                        <th>Nama Matakuliah</th>
                                        <th>SKS</th>
                                        <th>Prasyarat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($data_matakuliah as $key) {
                                        if($key->semester==6){ 

                                            if(in_array($key->kode_mk, $data_mk)){
                                                $checked = "checked";
                                            } else {
                                                $checked = "";
                                            }                                          
                                    ?>
                                    <tr>
                                        <td><input type="checkbox" disabled <?php echo $checked; ?> name="kode_mk[]" value="<?php echo $key->kode_mk; ?>"  <?php echo $status_form; ?>></td>
                                        <td><?php echo $key->nama_mk; ?></td>
                                        <td><?php echo $key->sks; ?></td>
                                        <td><?php echo $key->prasyarat_mk; ?></td>
                                    </tr>
                                    <?php
                                        }}
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class="row">
                                <div class="col-lg-12" align="Left">
                                <p style="font-size: 20px;">
                                    Semester 7</p>
                                </div>
                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>√</th>
                                        <th>Nama Matakuliah</th>
                                        <th>SKS</th>
                                        <th>Prasyarat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($data_matakuliah as $key) {
                                        if($key->semester==7){   

                                            if(in_array($key->kode_mk, $data_mk)){
                                                $checked = "checked";
                                            } else {
                                                $checked = "";
                                            }                                        
                                    ?>
                                    <tr>
                                        <td><input type="checkbox" disabled <?php echo $checked; ?> name="kode_mk[]" value="<?php echo $key->kode_mk; ?>"  <?php echo $status_form; ?>></td>
                                        <td><?php echo $key->nama_mk; ?></td>
                                        <td><?php echo $key->sks; ?></td>
                                        <td><?php echo $key->prasyarat_mk; ?></td>
                                    </tr>
                                    <?php
                                        }}
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class="row">
                                <div class="col-lg-12" align="Left">
                                <p style="font-size: 20px;">
                                    Semester 8</p>
                                </div>
                        </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>√</th>
                                        <th>Nama Matakuliah</th>
                                        <th>SKS</th>
                                        <th>Prasyarat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($data_matakuliah as $key) {
                                        if($key->semester==8){   

                                            if(in_array($key->kode_mk, $data_mk)){
                                                $checked = "checked";
                                            } else {
                                                $checked = "";
                                            }                                        
                                    ?>
                                    <tr>
                                        <td><input type="checkbox" disabled <?php echo $checked; ?> name="kode_mk[]" value="<?php echo $key->kode_mk; ?>"  <?php echo $status_form; ?>></td>
                                        <td><?php echo $key->nama_mk; ?></td>
                                        <td><?php echo $key->sks; ?></td>
                                        <td><?php echo $key->prasyarat_mk; ?></td>
                                    </tr>
                                    <?php
                                        }}
                                    ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            </form>
        </div>