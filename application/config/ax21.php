<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
 

/*
| -------------------------------------------------------------------------
| Config
| -------------------------------------------------------------------------
|
|
*/

$config['site_title']           		= 'Apps - Penilaian';
$config['site_copyright']       		= 'SyaefulAxl';
$config['site_years']       			= '2018';
$config['site_google']          		= 'https://plus.google.com/+SyaefulAxl';
$config['site_email']         		  	= 'syaefulaxl@gmail.com';
$config['site_title_delimiter'] 		= '-';
$config['credits_link']         		= 'http://syaefulaxl.com';
$config['version']    					= "v1.0.0";


$config['backend_mini_title']           = 'TEXWOR';
$config['backend_full_title']           = 'PT TEXCOMS WORLDWIDE';
?>