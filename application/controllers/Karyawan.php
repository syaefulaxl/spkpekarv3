<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->helper('file');
        $this->load->helper('url');
        $this->load->model('auth_model');
        $this->load->model('users_model');
        $this->load->model('karyawan_model');
        $this->load->library('form_validation');

        $username = $this->session->userdata('username');
		$username = array('username' => $username );

		$check = $this->auth_model->check("users", $username)->result();

		$access = array('Admin');

		if(!in_array($check[0]->status_login, $access)){
			redirect(base_url("auth/logout"));

		}

		if($check[0]->status == "Tidak Aktif"){
			redirect(base_url("auth/logout"));

		}

    }

	public function index()
	{

		$username = $this->session->userdata('username');

		$where = array('username' => $username);
		$data = $this->auth_model->user_data("users",$where)->result();

		$data['name'] = $data[0]->nama;
		$data['status_login'] = $data[0]->status_login;

		$data['data_karyawan'] = $this->karyawan_model->karyawan_depart();

		$idMax = $this->karyawan_model->data_users();
		$noUrut =(int) substr($idMax[0]->maxID,2,3);
        $noUrut ++;
        $data['newID']="US".sprintf("%03s",$noUrut);

		$this->load->view('layouts/header');
		$this->load->view('layouts/navigation', $data);
		$this->load->view('admin/karyawan', $data);
		$this->load->view('layouts/footer');


			
	}

	public function add()
	{

		$this->form_validation->set_rules("emp", "EMP", "trim|required");
		$this->form_validation->set_rules("nama_kary", "Nama karyawan", "trim|required");
		$this->form_validation->set_rules("posisi", "Posisi Jabatan", "trim|required");
		$this->form_validation->set_rules("dept_id", "Department", "trim|required");
		$this->form_validation->set_rules("kelamin_kary", "Kelamin Karyawan", "trim|required");
		$this->form_validation->set_rules("email_kary", "E-Mail karyawan", "trim|required");
		$this->form_validation->set_rules("tanggal_lahir_kary", "Tanggal Lahir karyawan", "trim|required");



		if ($this->form_validation->run() == true){

			$emp = $this->input->post('emp');
			$nama_kary = $this->input->post('nama_kary');
			$posisi = $this->input->post('posisi');
			$dept_id = $this->input->post('dept_id');
			$kelamin_kary = $this->input->post('kelamin_kary');
			$email_kary = $this->input->post('email_kary');
			$tanggal_lahir_kary = $this->input->post('tanggal_lahir_kary');

			$emp_find = array('emp' => $emp);
			$cek_emp = $this->karyawan_model->validate_username("tb_karyawan",$emp_find)->num_rows();

			$email_find = array('email_kary' => $email_kary);
			$cek_email = $this->karyawan_model->validate_email("tb_karyawan",$email_find)->num_rows();

			if ($cek_emp > 0) {
				echo "<script>alert('EMP sudah digunakan !')</script>";
				echo "<script>history.back(1)</script>";
				
			} else if ($cek_email > 0) {
				echo "<script>alert('E-mail sudah digunakan !')</script>";
				echo "<script>history.back(1)</script>";
				
			} else {

			$data 	  = array(
				'emp' => $emp, 
				'nama_kary' => $nama_kary,
				'posisi' => $posisi,
				'dept_id' => $dept_id,
				'kelamin_kary' => $kelamin_kary, 
				'email_kary' => $email_kary, 
				'tanggal_lahir_kary' => $tanggal_lahir_kary, 
				'status' => 'Aktif'); 

			$insert = $this->karyawan_model->insert('tb_karyawan',$data);

			echo "<script>alert('Data karyawan berhasil di simpan.')</script>";
			echo "<script>window.location='../karyawan'</script>";
			}

		} else {

			echo "<script>alert('Data karyawan Gagal Di Simpan.')</script>";
			echo "<script>window.location='../karyawan'</script>";
		
		} 

	}

	public function update()
	{

		$this->form_validation->set_rules("eemp", "emp", "trim|required");
		$this->form_validation->set_rules("enama_kary", "Nama karyawan", "trim|required");
		$this->form_validation->set_rules("eposisi", "posisi Kelas", "trim|required");
		$this->form_validation->set_rules("edept_id", "posisi Studi", "trim|required");
		$this->form_validation->set_rules("ekelamin_kary", "Kelamin karyawan", "trim|required");
		$this->form_validation->set_rules("eemail_kary", "E-Mail karyawan", "trim|required");
		$this->form_validation->set_rules("etanggal_lahir_kary", "Tanggal Lahir karyawan", "trim|required");



		if ($this->form_validation->run() == true){

			$emp = $this->input->post('eemp');
			$nama_kary = $this->input->post('enama_kary');
			$posisi = $this->input->post('eposisi');
			$dept_id = $this->input->post('edept_id');
			$kelamin_kary = $this->input->post('ekelamin_kary');
			$email_kary = $this->input->post('eemail_kary');
			$tanggal_lahir_kary = $this->input->post('etanggal_lahir_kary');

			$data 	  = array(
				'emp' => $emp,
				'nama_kary' => $nama_kary,
				'posisi' => $posisi,
				'dept_id' => $dept_id,
				'kelamin_kary' => $kelamin_kary, 
				'email_kary' => $email_kary, 
				'tanggal_lahir_kary' => $tanggal_lahir_kary, 
				'status' => 'Aktif');  

			$insert = $this->karyawan_model->update('tb_karyawan',$emp,$data);

			echo "<script>alert('Data karyawan Berhasil Di Update.')</script>";
			echo "<script>window.location='../karyawan'</script>";
			
		} else {

			echo "<script>alert('Data karyawan Gagal Di Update.')</script>";
			echo "<script>window.location='../karyawan'</script>";
		
		} 
			
	}

	public function update_status()
	{

		$emp = $this->input->get('emp');
		$where_status = array('emp' => $emp);

		$status = $this->karyawan_model->check_status("tb_karyawan",$where_status)->result();

		if ($status[0]->status == "Tidak Aktif"){
			$data 	  = array('status' => 'Aktif');
			$message  = "Di Akfikan !"; 

		} else {
			$data 	  = array('status' => 'Tidak Aktif');
			$message  = "Di Nonaktifkan !"; 
		}

		$this->karyawan_model->update_status('tb_karyawan',$emp,$data);

		echo "<script>alert('Data karyawan berhasil $message')</script>";
		echo "<script>window.location='../karyawan'</script>";
			
	}


	public function delete()
	{

		$emp = $this->input->get('emp');
		$check_karyawan = array('emp' => $emp);
		$check_data = $this->karyawan_model->check_data("tb_karyawan", $check_karyawan)->num_rows();

		if ($check_data == 1){
			$this->karyawan_model->delete('tb_karyawan',$emp);

			echo "<script>alert('Data karyawan berhasil di hapus')</script>";
			echo "<script>window.location='../karyawan'</script>";

		} else {
			echo "<script>alert('Data karyawan tidak bisa di hapus, karena telah berkaitan dengan data lain !')</script>";
			echo "<script>window.location='../karyawan'</script>";

		}

	}

	public function import_emp()
	{

		$this->form_validation->set_rules("iemp", "Kode Karyawan", "trim|required");

		if ($this->form_validation->run() == true){

			$emp = $this->input->post('iemp');

			$config['upload_path'] =  './temp/';
			$config['allowed_types'] = 'xlsx|csv|xls';

			$this->load->library('upload', $config);


		if ( ! $this->upload->do_upload('5file')){
			echo "<script>alert('upload gagal atau type file salah')</script>";
			echo "<script>window.location='../karyawan'</script>";
			
		} else {
			$upload_data = $this->upload->data();
			$file_name = $upload_data['file_name'];

			$this->load->library("Excel");
		
			$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load('temp/'.$file_name);
			$sheetnumber = 0;

				foreach ($objPHPExcel->getWorksheetIterator() as $sheet){
		
					$s = $sheet->getTitle();

					$sheet= str_replace(' ', '', $s);
					$sheet= strtolower($sheet); 
					$objWorksheet = $objPHPExcel->getSheetByName($s);
			
					$lastRow = $objPHPExcel->setActiveSheetIndex($sheetnumber)->getHighestRow(); 
					$sheetnumber++;
		
					if($sheet=='karyawan'){

						for($j=4; $j<=$lastRow; $j++){
	     
				  
			   				$emp = $objWorksheet->getCellByColumnAndRow(0,$j)->getValue();
			   				$nama_kary = $objWorksheet->getCellByColumnAndRow(1,$j)->getValue();
		        			$posisi = $objWorksheet->getCellByColumnAndRow(2,$j)->getValue();
		        			$dept_id = $objWorksheet->getCellByColumnAndRow(3,$j)->getValue();
		        			$email_kary = $objWorksheet->getCellByColumnAndRow(4,$j)->getValue();
		        			$tanggal_lahir_kary = $objWorksheet->getCellByColumnAndRow(5,$j)->getValue();

		        			// The date 6/30/2009 is stored as 39994 in Excel 
							$days = $tanggal_lahir_kary; 

							// But you must subtract 1 to get the correct timestamp 
							$ts = mktime(0,0,0,1,$days-1,1900); 

							// So, this would then match Excel’s representation: 
							$the_birthdate =  date("Y-m-d",$ts);  
							
       
							if($emp != '' || $nama_kary != ''){


								$idMax = $this->karyawan_model->karyawan_emp();
								$noUrut =(int) substr($idMax[0]->maxID,2,3);
        						$noUrut ++;
        						$newID ="EMP".sprintf("%03s",$noUrut);

			        			$excel = array(
			        				'emp'=>$emp,
			        				'nama_kary'=>$nama_kary,
			        				'posisi'=>$posisi,
			        				'dept_id'=>$dept_id,
			        				'email_kary'=>$email_kary,
			        				'tanggal_lahir_kary'=>$the_birthdate,
			        			);

			        				$check_emp = array('emp' => $emp);
			        				$cek_emp = $this->karyawan_model->check_data('tb_karyawan',$check_emp)->num_rows();

			        				if ($cek_emp == 0){

									$this->db->insert('tb_karyawan',$excel);

									}

									$result=($this->db->affected_rows()!= 1)? false:true;

									// var_dump($result);
									// var_dump($upload_data['file_path']);

									// die();

									delete_files($upload_data['file_path']);

									if($result == true){
							
										echo "<script>alert('upload berhasil data tersimpan')</script>";
										echo "<script>window.location='../karyawan'</script>";
									
									} else {
										echo "<script>alert('upload gagal atau kode sudah terpakai semua')</script>";
										echo "<script>window.location='../karyawan'</script>";
						
									}

					
			      			} else {
								echo "<script>alert('upload gagal data tidak sesuai')</script>";
								echo "<script>window.location='../karyawan'</script>";
					
							}		
						}	
						
					}
				
				}

				echo "<script>alert('Upload Gagal atau Data Tidak Sesuai dengan Format')</script>";
				echo "<script>window.location='../karyawan'</script>";
		
		}

		} else {echo "<script>alert('Upload Data Berhasil')</script>";
			echo "<script>window.location='../Karyawan'</script>";
		
		} 
		
	}
}
