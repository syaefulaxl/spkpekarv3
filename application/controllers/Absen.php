<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absen extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');
        $this->load->model('absen_model');
        $this->load->library('form_validation');

        $username = $this->session->userdata('username');
		$username = array('username' => $username );

		$check = $this->auth_model->check("users", $username)->result();

		$access = array('Admin');

		if(!in_array($check[0]->status_login, $access)){
			redirect(base_url("auth/logout"));

		}

		if($check[0]->status == "Tidak Aktif"){
			redirect(base_url("auth/logout"));

		}

    }

	public function index()
	{

		$username = $this->session->userdata('username');

		$where = array('username' => $username);
		$data = $this->auth_model->user_data("users",$where)->result();

		$data['name'] = $data[0]->nama;
		$data['status_login'] = $data[0]->status_login;

		$data['data_absen'] = $this->absen_model->absen();
		$data['data_matakuliah'] = $this->absen_model->table_data("tb_matakuliah")->result();
		$data['data_dosen'] = $this->absen_model->table_data("tb_dosen")->result();
		$idMax = $this->absen_model->data_users();
		$noUrut =(int) substr($idMax[0]->maxID,2,3);
        $noUrut ++;
        $data['newID']="US".sprintf("%03s",$noUrut);

		$this->load->view('layouts/header');
		$this->load->view('layouts/navigation', $data);
		$this->load->view('admin/absen', $data);
		$this->load->view('layouts/footer');


			
	}

	public function add()
	{

		$this->form_validation->set_rules("kode_mk", "Kode Matakuliah", "trim|required");
		$this->form_validation->set_rules("nidn", "Nama Dosen", "trim|required");
		$this->form_validation->set_rules("hari", "Hari", "trim|required");
		$this->form_validation->set_rules("jamul", "Jam Mulai", "trim|required");
		$this->form_validation->set_rules("jamhir", "Jam Akhir", "trim|required");

		if ($this->form_validation->run() == true){

			$kode_mk = $this->input->post('kode_mk');
			$nidn = $this->input->post('nidn');
			$hari = $this->input->post('hari');
			$jamul = $this->input->post('jamul');
			$jamhir = $this->input->post('jamhir');

			$kode_mk_find = array('kode_mk' => $kode_mk);
			$cek_kode_mk = $this->absen_model->validate_username("tb_absen",$kode_mk_find)->num_rows();

			$nidn_find = array('nidn' => $nidn);
			$cek_nidn = $this->absen_model->validate_email("tb_absen",$nidn_find)->num_rows();

			if ($cek_kode_mk > 0 && $cek_nidn > 0) {
				echo "<script>alert('Absen sudah ada !')</script>";
				echo "<script>history.back(1)</script>";
				
			} else {

			$data 	  = array(
			'kode_mk' => $kode_mk,
			'nidn' => $nidn,
			'hari' => $hari, 
			'jamul' => $jamul, 
			'jamhir' => $jamhir);

			$insert = $this->absen_model->insert('tb_absen',$data);

			echo "<script>alert('Data Absen berhasil di simpan.')</script>";
			echo "<script>window.location='../absen'</script>";
			}

		} else {

			echo "<script>window.location='../absen'</script>";
		
		} 

	}

	public function update()
	{

		$this->form_validation->set_rules("kode_mk", "Kode Matakuliah", "trim|required");
		$this->form_validation->set_rules("nidn", "Nama Dosen", "trim|required");
		$this->form_validation->set_rules("hari", "Hari", "trim|required");
		$this->form_validation->set_rules("jamul", "Jam Mulai", "trim|required");
		$this->form_validation->set_rules("jamhir", "Jam Akhir", "trim|required");

		if ($this->form_validation->run() == true){

			$absen_id = $this->input->post('absen_id');
			$kode_mk = $this->input->post('kode_mk');
			$nidn = $this->input->post('nidn');
			$hari = $this->input->post('hari');
			$jamul = $this->input->post('jamul');
			$jamhir = $this->input->post('jamhir');
				
			$data 	  = array(
			'kode_mk' => $kode_mk,
			'nidn' => $nidn,
			'hari' => $hari, 
			'jamul' => $jamul, 
			'jamhir' => $jamhir);

			$insert = $this->absen_model->update('tb_absen',$absen_id,$data);

			echo "<script>alert('Data Absen berhasil di update.')</script>";
			echo "<script>window.location='../absen'</script>";

		} else {

			echo "<script>alert('Data Absen Gagal Di Update.')</script>";
			echo "<script>window.location='../absen'</script>";	
			
		}

	}

	public function delete()
	{


		$absen_id = $this->input->get('absen_id');
		$kode_mk = $this->input->get('kode_mk');
		$check_kode_mk = array('kode_mk' => $kode_mk);
		$nidn = $this->input->get('nidn');
		$check_nidn = array('nidn' => $nidn);
		$check_data = $this->absen_model->check_data("tb_absen", $check_kode_mk)->num_rows();
		$check_data = $this->absen_model->check_data("tb_absen", $check_nidn)->num_rows();

		if ($check_data == 0){
			$this->absen_model->delete('tb_absen',$absen_id);

			echo "<script>alert('Data Absen berhasil di hapus')</script>";
			echo "<script>window.location='../absen'</script>";

		} else {
			echo "<script>alert('Data Absen tidak bisa di hapus, karena telah berkaitan dengan data lain !')</script>";
			echo "<script>window.location='../absen'</script>";

		}

	}
}
