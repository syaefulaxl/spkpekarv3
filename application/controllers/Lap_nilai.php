<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lap_nilai extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');
        $this->load->model('users_model');
        $this->load->model('nilai_model');
        $this->load->model('kriteria_model');
        $this->load->library('form_validation');

		$username = $this->session->userdata('username');
		$username = array('username' => $username );

		$password = $this->session->userdata('password');
		$password = array('password' => $password );

		$check = $this->auth_model->check("users", $username)->result();

		$access = array('Admin','Direktur','Manajer');

		if(!in_array($check[0]->status_login, $access)){
			redirect(base_url("auth/logout"));

		}

		if($check[0]->status == "Tidak Aktif"){
			redirect(base_url("auth/logout"));

		}

    }

	public function index()
	{

		$username = $this->session->userdata('username');

		$where = array('username' => $username);
		$data = $this->auth_model->user_data("users",$where)->result();

		$data['name'] = $data[0]->nama;
		$data['status_login'] = $data[0]->status_login;

		$data['data_nilai'] = $this->nilai_model->get_laporan_nilai();
		$data['generate_url'] = site_url('Lap_nilai/generate_penilaian');
		$data['export_url'] = site_url('Lap_nilai/export_penilaian');

		$this->load->view('layouts/header');
		$this->load->view('layouts/navigation', $data);
		$this->load->view('laporan/penilaian', $data);
		$this->load->view('layouts/footer');


			
	}

	public function export_penilaian()
	{

		$username = $this->session->userdata('username');

		$where = array('username' => $username);
		$data = $this->auth_model->user_data("users",$where)->result();

		$data['name'] = $data[0]->nama;
		$data['status_login'] = $data[0]->status_login;

		$data['data_nilai'] = $this->nilai_model->get_laporan_nilai();

		$this->load->view('laporan/excel_penilaian', $data);
		
	}

	public function generate_penilaian()
	{

		$username = $this->session->userdata('username');

		$where = array('username' => $username);
		$data = $this->auth_model->user_data("users",$where)->result();

		$data['name'] = $data[0]->nama;
		$data['status_login'] = $data[0]->status_login;

		$data_nilai = $this->nilai_model->get_kary_nilai();
		$data_kriteria = $this->kriteria_model->table_data("tb_kriteria")->result();

		foreach ($data_kriteria as $data_kriteria_entry) {

			$where_kode_kriter = array('kode_kriter' => $data_kriteria_entry->kode_kriter);
			$check_kriteria = $this->kriteria_model->check_data("tb_kriteria",$where_kode_kriter)->result();

			if($data_kriteria_entry->kode_kriter == "K1"){

				if($check_kriteria[0]->atribut == "Benefit"){
					$check_max_k1_kriteria = $this->kriteria_model->check_max_k1();
					$max_k1 = $check_max_k1_kriteria[0]->max_k1;
					$divider_k1 = $max_k1;

				} else {
					$check_min_k1_kriteria = $this->kriteria_model->check_min_k1();
					$min_k1 = $check_min_k1_kriteria[0]->min_k1;
					$divider_k1 = $min_k1;

				}

				$atribut_k1 = $check_kriteria[0]->atribut;
				$divider_k1 = $divider_k1;
				$bobot_k1 = $data_kriteria_entry->bobot;

			} else if($data_kriteria_entry->kode_kriter == "K2") {

				if($check_kriteria[0]->atribut == "Benefit"){
					$check_max_k2_kriteria = $this->kriteria_model->check_max_k2();
					$max_k2 = $check_max_k2_kriteria[0]->max_k2;
					$divider_k2 = $max_k2;

				} else {
					$check_min_k2_kriteria = $this->kriteria_model->check_min_k2();
					$min_k2 = $check_min_k2_kriteria[0]->min_k2;
					$divider_k2 = $min_k2;

				}

				$atribut_k2 = $check_kriteria[0]->atribut;
				$divider_k2 = $divider_k2;
				$bobot_k2 = $data_kriteria_entry->bobot;

			} else if($data_kriteria_entry->kode_kriter == "K3") {

				if($check_kriteria[0]->atribut == "Benefit"){
					$check_max_k3_kriteria = $this->kriteria_model->check_max_k3();
					$max_k3 = $check_max_k3_kriteria[0]->max_k3;
					$divider_k3 = $max_k3;

				} else {
					$check_min_k3_kriteria = $this->kriteria_model->check_min_k3();
					$min_k3 = $check_min_k3_kriteria[0]->min_k3;
					$divider_k3 = $min_k3;

				}

				$atribut_k3 = $check_kriteria[0]->atribut;
				$divider_k3 = $divider_k3;
				$bobot_k3 = $data_kriteria_entry->bobot;
				
			} else if($data_kriteria_entry->kode_kriter == "K4") {

				if($check_kriteria[0]->atribut == "Benefit"){
					$check_max_k4_kriteria = $this->kriteria_model->check_max_k4();
					$max_k4 = $check_max_k4_kriteria[0]->max_k4;
					$divider_k4 = $max_k4;

				} else {
					$check_min_k4_kriteria = $this->kriteria_model->check_min_k4();
					$min_k4 = $check_min_k4_kriteria[0]->min_k4;
					$divider_k4 = $min_k4;

				}

				$atribut_k4 = $check_kriteria[0]->atribut;
				$divider_k4 = $divider_k4;
				$bobot_k4 = $data_kriteria_entry->bobot;
				
			} else if($data_kriteria_entry->kode_kriter == "K5") {

				if($check_kriteria[0]->atribut == "Benefit"){
					$check_max_k5_kriteria = $this->kriteria_model->check_max_k5();
					$max_k5 = $check_max_k5_kriteria[0]->max_k5;
					$divider_k5 = $max_k5;

				} else {
					$check_min_k5_kriteria = $this->kriteria_model->check_min_k5();
					$min_k5 = $check_min_k5_kriteria[0]->min_k5;
					$divider_k5 = $min_k5;

				}

				$atribut_k5 = $check_kriteria[0]->atribut;
				$divider_k5 = $divider_k5;
				$bobot_k5 = $data_kriteria_entry->bobot;

			} else {

				if($check_kriteria[0]->atribut == "Benefit"){
					$check_max_absen_kriteria = $this->kriteria_model->check_max_absen();
					$max_absen = $check_max_absen_kriteria[0]->max_absen;
					$divider_absen = $max_absen;

				} else {
					$check_min_absen_kriteria = $this->kriteria_model->check_min_absen();
					$min_absen = $check_min_absen_kriteria[0]->min_absen;
					$divider_absen = $min_absen;

				}

				$atribut_absen = $check_kriteria[0]->atribut;
				$divider_absen = $divider_absen;
				$bobot_absen = $data_kriteria_entry->bobot;

			}

		}


		foreach ($data_nilai as $data_nilai_entry) {

			if ($atribut_k1 == "Benefit"){
				$nilai_total_k1 = ($data_nilai_entry->k1/$divider_k1) * $bobot_k1;
			} else {
				$nilai_total_k1 = ($divider_k1/$data_nilai_entry->k1) * $bobot_k1;
			}

			if ($atribut_k2 == "Benefit"){
				$nilai_total_k2 = ($data_nilai_entry->k2/$divider_k2) * $bobot_k2;
			} else {
				$nilai_total_k2 = ($divider_k2/$data_nilai_entry->k2) * $bobot_k2;
			}

			if ($atribut_k3 == "Benefit"){
				$nilai_total_k3 = ($data_nilai_entry->k3/$divider_k3) * $bobot_k3;
			} else {
				$nilai_total_k3 = ($divider_k3/$data_nilai_entry->k3) * $bobot_k3;
			}

			if ($atribut_k4 == "Benefit"){
				$nilai_total_k4 = ($data_nilai_entry->k4/$divider_k4) * $bobot_k4;
			} else {
				$nilai_total_k4 = ($divider_k4/$data_nilai_entry->k4) * $bobot_k4;
			}

			if ($atribut_k5 == "Benefit"){
				$nilai_total_k5 = ($data_nilai_entry->k5/$divider_k5) * $bobot_k5;
			} else {
				$nilai_total_k5 = ($divider_k5/$data_nilai_entry->k5) * $bobot_k5;
			}

			if ($atribut_absen == "Benefit"){
				$nilai_total_absen = ($data_nilai_entry->absen/$divider_absen) * $bobot_absen;
			} else {
				$nilai_total_absen = ($divider_absen/$data_nilai_entry->absen) * $bobot_absen;
			}

			$total_nilai = $nilai_total_k1+$nilai_total_k2+$nilai_total_k3+$nilai_total_k4+$nilai_total_k5+$nilai_total_absen;

			if ($total_nilai >= 95){
				$deskripsi = "Rekomendasi Naik Jabatan";

			} else if ($total_nilai < 95 AND $total_nilai >= 65) {
				$deskripsi = "Pertahankan Posisi";

			} else if ($total_nilai < 65 AND $total_nilai >= 50) {
				$deskripsi = "Mutasi";

			} else if ($total_nilai < 50 AND $total_nilai >= 30) {
				$deskripsi = "Pemanggilan";

			} else {
				$deskripsi = "PHK";

			}

			$data_nilai = array(
				'total' => $total_nilai, 
				'deskripsi' => $deskripsi, 
			);

			$update_nilai = $this->nilai_model->update_penilaian("tb_nilai_detail",$data_nilai_entry->nilai_id,$data_nilai);

			echo "<script>alert('Laporan Penilaian berhasil dihitung.')</script>";
			echo "<script>window.location='../Lap_nilai'</script>";
		}
		
	}
}