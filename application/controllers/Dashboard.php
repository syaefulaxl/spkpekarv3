<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');
        $this->load->model('users_model');
        $this->load->library('form_validation');

		$username = $this->session->userdata('username');
		$username = array('username' => $username );

		$password = $this->session->userdata('password');
		$password = array('password' => $password );

		$check = $this->auth_model->check("users", $username)->result();

		$access = array('Admin','Direktur','Manajer');

		if(!in_array($check[0]->status_login, $access)){
			redirect(base_url("auth/logout"));

		}

		if($check[0]->status == "Tidak Aktif"){
			redirect(base_url("auth/logout"));

		}

    }

	public function index()
	{
		$username = $this->session->userdata('username');

		$where = array('username' => $username);
		$data = $this->auth_model->user_data("users",$where)->result();

		$data['name'] = $data[0]->nama;
		$data['status_login'] = $data[0]->status_login;
		$data['data_nilai'] = $this->users_model->user_data("tb_nilai")->result();

		$this->load->view('layouts/header');
		$this->load->view('layouts/navigation', $data);
		$this->load->view('root/dashboard', $data);
		$this->load->view('layouts/footer');
			
	}
}
