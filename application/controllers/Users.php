<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');
        $this->load->model('users_model');
        $this->load->library('form_validation');

        $username = $this->session->userdata('username');
		$username = array('username' => $username );

		$check = $this->auth_model->check("users", $username)->result();

		$access = array('Admin');

		if(!in_array($check[0]->status_login, $access)){
			redirect(base_url("auth/logout"));

		}

		if($check[0]->status == "Tidak Aktif"){
			redirect(base_url("auth/logout"));

		}

    }

	public function index()
	{

		$username = $this->session->userdata('username');

		$where = array('username' => $username);
		$data = $this->auth_model->user_data("users",$where)->result();

		$data['name'] = $data[0]->nama;
		$data['status_login'] = $data[0]->status_login;

		$data['data_users'] = $this->users_model->user_data("users")->result();
		$idMax = $this->users_model->users_username();
		$noUrut =(int) substr($idMax[0]->maxID,2,3);
        $noUrut ++;
        $data['newID']="US".sprintf("%03s",$noUrut);

		$this->load->view('layouts/header');
		$this->load->view('layouts/navigation', $data);
		$this->load->view('admin/users', $data);
		$this->load->view('layouts/footer');


			
	}

	public function add()
	{

		$this->form_validation->set_rules("username", "Username", "trim|required");
		$this->form_validation->set_rules("nama", "Nama", "trim|required");
		$this->form_validation->set_rules("email", "E-mail", "trim|required");
		$this->form_validation->set_rules("password", "Password", "trim|required");
		$this->form_validation->set_rules("status_login", "Status Akses", "trim|required");

		if ($this->form_validation->run() == true){

			$username = $this->input->post('username');
			$nama = $this->input->post('nama');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$password = sha1($password);
			$status_login = $this->input->post('status_login');

			$username_find = array('username' => $username);
			$cek_username = $this->users_model->validate_username("users",$username_find)->num_rows();

			$email_find = array('email' => $email);
			$cek_email = $this->users_model->validate_email("users",$email_find)->num_rows();

			if ($cek_username > 0) {
				echo "<script>alert('Username sudah digunakan !')</script>";
				echo "<script>history.back(1)</script>";
				
			} else if ($cek_email > 0) {
				echo "<script>alert('E-mail sudah digunakan !')</script>";
				echo "<script>history.back(1)</script>";
				
			} else if ($status_login == "0") {
				echo "<script>alert('Mohon pilih status akses terlebih dahulu !')</script>";
				echo "<script>history.back(1)</script>";
				
			} else {

			$data 	  = array('username' => $username, 'nama' => $nama, 'email' => $email, 'password' => $password, 'status_login' => $status_login, 'status' => 'Aktif'); 

			$insert = $this->users_model->insert('users',$data);

			echo "<script>alert('Data pengguna berhasil di simpan.')</script>";
			echo "<script>window.location='../users'</script>";
			}

		} else {

			echo "<script>window.location='../users'</script>";
		
		} 

	}

	public function update()
	{

		$this->form_validation->set_rules("eusername", "Kode Pengguna", "trim|required");
		$this->form_validation->set_rules("enama", "Nama", "trim|required");
		$this->form_validation->set_rules("epassword", "Password", "trim|required");
		$this->form_validation->set_rules("estatus_login", "Status Akses", "trim|required");

		if ($this->form_validation->run() == true){

			$username = $this->input->post('eusername');
			$nama = $this->input->post('enama');
			$password = $this->input->post('epassword');
			$password = sha1($password);
			$status_login = $this->input->post('estatus_login');

			if ($status_login == "0") {
				echo "<script>alert('Mohon pilih status akses terlebih dahulu !')</script>";
				echo "<script>history.back(1)</script>";
				
			} else {

			$data 	  = array('nama' => $nama, 'password' => $password, 'status_login' => $status_login); 

			$insert = $this->users_model->update('users',$username,$data);

			echo "<script>alert('Data pengguna berhasil di update.')</script>";
			echo "<script>window.location='../users'</script>";
			}

		} else {

			echo "<script>window.location='../users'</script>";
		
		} 
			
	}

	public function update_status()
	{

		$username = $this->input->get('username');
		$where_status = array('username' => $username);

		$status = $this->users_model->check_status("users",$where_status)->result();

		if ($status[0]->status == "Tidak Aktif"){
			$data 	  = array('status' => 'Aktif');
			$message  = "Di Akfikan !"; 

		} else {
			$data 	  = array('status' => 'Tidak Aktif');
			$message  = "Di Nonaktifkan !"; 
		}

		$this->users_model->update('users',$username,$data);

		echo "<script>alert('Data pengguna berhasil $message')</script>";
		echo "<script>window.location='../users'</script>";
			
	}


	public function delete()
	{

		$username = $this->input->get('username');
		$check_pengguna = array('username' => $username);
		$check_data = $this->users_model->check_data("users", $check_pengguna)->num_rows();

		if ($check_data == 1){
			$this->users_model->delete('users',$username);

			echo "<script>alert('Data pengguna berhasil di hapus')</script>";
			echo "<script>window.location='../users'</script>";

		} else {
			echo "<script>alert('Data pengguna tidak bisa di hapus, karena telah berkaitan dengan data lain !')</script>";
			echo "<script>window.location='../users'</script>";

		}

	}
}
