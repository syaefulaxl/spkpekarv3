<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');
        $this->load->model('users_model');
         $this->load->model('nilai_model');
        $this->load->library('form_validation');

        $username = $this->session->userdata('username');
		$username = array('username' => $username );

		$check = $this->auth_model->check("users", $username)->result();

		$access = array('Admin','Manajer','Direktur');

		if(!in_array($check[0]->status_login, $access)){
			redirect(base_url("auth/logout"));

		}

		if($check[0]->status == "Tidak Aktif"){
			redirect(base_url("auth/logout"));

		}

    }

	public function index()
	{

		$username = $this->session->userdata('username');

		$where = array('username' => $username);
		$data = $this->auth_model->user_data("users",$where)->result();

		$data['name'] = $data[0]->nama;
		$data['status_login'] = $data[0]->status_login;

		$where_nilai = array('emp' => $username);

		$data['data_nilai'] = $this->nilai_model->get_kary_nilai();
		$data['data_nilai_2'] = $this->nilai_model->get_kary_nilai_2();

		$this->load->view('layouts/header');
		$this->load->view('layouts/navigation', $data);
		$this->load->view('karyawan/nilai', $data);
		$this->load->view('layouts/footer');


			
	}

	public function get_data_emp()
	{
		$nama_kary = $this->input->post('nama_kary0') or $this->input->post('nama_kary');

		 // var_dump($nama_kary);

        $data_emp = $this->nilai_model->get_emp_by_name($nama_kary);

        if (!empty($data_emp)){
 			$valid_emp = true;
 			$emp_value = $data_emp[0]->emp;
        } else {
        	$valid_emp = false;
 			$emp_value = "";
        }

        $result = array(
        	'valid_emp' => $valid_emp,
            'emp_value' => $emp_value,
        );

        echo json_encode($result);
		

	}

	public function add_nilai()
	{

		$this->form_validation->set_rules("emp", "EMP", "trim|required");
		$this->form_validation->set_rules("nama_kary", "Nama karyawan", "trim|required");
		$this->form_validation->set_rules("tgl_nilai", "Tanggal nilai", "trim|required");
		$this->form_validation->set_rules("k1", "Kriteria 1", "trim|required");
		$this->form_validation->set_rules("k2", "Kriteria 2", "trim|required");
		$this->form_validation->set_rules("k3", "Kriteria 3", "trim|required");
		$this->form_validation->set_rules("k4", "Kriteria 4", "trim|required");
		$this->form_validation->set_rules("k5", "Kriteria 5", "trim|required");
		$this->form_validation->set_rules("absen", "Absen", "trim|required");

		if ($this->form_validation->run() == true){

			$username = $this->session->userdata('username');

			$emp = $this->input->post('emp');
			$nama_kary = $this->input->post('nama_kary');
			$tgl_nilai = date('Y-m-d');
			$k1 = $this->input->post('k1');
			$k2 = $this->input->post('k2');
			$k3 = $this->input->post('k3');
			$k4 = $this->input->post('k4');
			$k5 = $this->input->post('k5');
			$absen = $this->input->post('absen');

			$reg_ID = "NL";
            $idMax = $this->nilai_model->nilai_id_code($reg_ID);
            $noUrut = (int) substr($idMax[0]->maxID,2,4);
            $noUrut ++;
            $newID = $reg_ID.sprintf("%04s",$noUrut);

			$data0	  = array(	
			'nilai_id' => $newID,
			'emp' => $emp, 
			'tgl_nilai' => $tgl_nilai);	 

			$data1 	  = array(
				'nilai_id' => $newID,
				'k1' => $k1,
				'k2' => $k2, 
				'k3' => $k3, 
				'k4' => $k4, 
				'k5' => $k5,
				'absen' => $k5);

			$insert0 = $this->nilai_model->insert('tb_nilai',$data0);
			$insert1 = $this->nilai_model->insert('tb_nilai_detail',$data1);

			echo "<script>alert('Data karyawan berhasil di simpan.')</script>";
			echo "<script>window.location='../nilai'</script>";
			
		} else {

			echo "<script>alert('Data nilai Gagal Di Simpan.')</script>";
			echo "<script>window.location='../nilai'</script>";
		
		} 
			
	}

	public function update_nilai()
	{

		$this->form_validation->set_rules("enilai_id", "", "trim|required");
		$this->form_validation->set_rules("esemester", "", "trim|required");


		if ($this->form_validation->run() == true){

			$id_nilai = $this->input->post('enilai_id');
			$tgl_nilai = date('Y-m-d');
			$semester = $this->input->post('esemester');

			$data_nilai  = array(
				'tgl_nilai' => $tgl_nilai,
				'semester' => $semester);  

			$insert = $this->nilai_model->update_nilai('tb_nilai',$id_nilai,$data_nilai);

			echo "<script>alert('Data nilai Berhasil Di Update.')</script>";
			echo "<script>window.location='../nilai'</script>";
			
		} else {

			echo "<script>alert('Data nilai Gagal Di Update.')</script>";
			echo "<script>window.location='../nilai'</script>";
		
		} 
			
	}

	public function update_status()
	{

		$username = $this->input->get('username');
		$where_status = array('username' => $username);

		$status = $this->users_model->check_status("users",$where_status)->result();

		if ($status[0]->status == "Tidak Aktif"){
			$data 	  = array('status' => 'Aktif');
			$message  = "Di Akfikan !"; 

		} else {
			$data 	  = array('status' => 'Tidak Aktif');
			$message  = "Di Nonaktifkan !"; 
		}

		$this->users_model->update('users',$username,$data);

		echo "<script>alert('Data pengguna berhasil $message')</script>";
		echo "<script>window.location='../users'</script>";
			
	}


	
	public function delete_nilai()
	{

		$nilai_id = $this->input->get('nilai_id');
		$check_nilai_id = array('nilai_id' => $nilai_id);
		$check_data = $this->nilai_model->check_data("tb_nilai", $check_nilai_id)->num_rows();

		if ($check_data == 1){
			$this->nilai_model->delete_nilai('tb_nilai',$nilai_id);

			echo "<script>alert('Data nilai berhasil di hapus')</script>";
			echo "<script>window.location='../nilai'</script>";

		} else {
			echo "<script>alert('Data nilai tidak bisa di hapus, karena telah berkaitan dengan data lain !')</script>";
			echo "<script>window.location='../nilai'</script>";

		}

	}

	public function update_perwalian()
	{	

		$nilai_id = $this->input->post('nilai_id');
		$status = $this->input->post('status');

		$this->form_validation->set_rules("nilai_id", "", "trim|required");
		$this->form_validation->set_rules("status", "", "trim|required");

		if ($this->form_validation->run() == true){
			
			$kode_mk = $this->input->post('kode_mk');

			if (!empty($kode_mk) AND $status == "Belum Disetujui"){

				$delete_mk = $this->nilai_model->delete_nilai('tb_nilai_detail',$nilai_id);

				foreach ($kode_mk as $kode_value) {

					$data_mk  = array(
						'nilai_id' => $nilai_id,
						'kode_mk' => $kode_value); 

					$insert_mk = $this->nilai_model->insert('tb_nilai_detail',$data_mk);

				} 
				
				echo "<script>alert('Matakuliah Pada nilai ini Telah Disimpan !')</script>";
				echo "<script>window.location='../nilai'</script>";
				//echo "<script>history.back(1)</script>";
				die();

			} else {
				echo "<script>alert('Anda Belum Memilih Apapun !')</script>";
				echo "<script>history.back(1)</script>";
				die();
			}

			
		} else {
			echo "<script>alert('Anda tidak memilih nilai terlebih dahulu.')</script>";
			echo "<script>window.location='../nilai'</script>";
			die();
		}
	}

	public function mhs_nilai()
	{
		$nilai_id = $this->input->get('nilai_id');
		$status = $this->input->get('status');

		$username = $this->session->userdata('username');

		$where = array('username' => $username);
		$data = $this->auth_model->user_data("users",$where)->result();

		$data['name'] = $data[0]->nama;
		$data['status_login'] = $data[0]->status_login;

		$data['data_nilai'] = $this->nilai_model->table_data("tb_nilai")->result();
		$data['data_perwalian'] = $this->nilai_model->get_mhs_nilai();

		$data['data_nilai'] = $this->nilai_model->get_mhs_nilai();

		$data['nilai_id'] = $nilai_id;
		$data['status'] = $status;

		$this->load->view('layouts/header');
		$this->load->view('layouts/navigation', $data);
		$this->load->view('prodi/mhs_nilai', $data);
		$this->load->view('layouts/footer');

	}

	public function mhs_nilai_list()
	{
		$nilai_id = $this->input->get('nilai_id');
		$status = $this->input->get('status');

		$username = $this->session->userdata('username');

		$where = array('username' => $username);
		$data = $this->auth_model->user_data("users",$where)->result();

		$data['name'] = $data[0]->nama;
		$data['status_login'] = $data[0]->status_login;

		$data['data_nilai'] = $this->nilai_model->table_data("tb_nilai")->result();
		$data['data_matakuliah'] = $this->nilai_model->table_data("tb_matakuliah")->result();

		$data['data_perwalian'] = $this->nilai_model->get_nilai_detail($nilai_id);

		$data['data_nilai'] = $this->nilai_model->get_mhs_nilai();

		$data['nilai_id'] = $nilai_id;
		$data['status'] = $status;

		$this->load->view('layouts/header');
		$this->load->view('layouts/navigation', $data);
		$this->load->view('prodi/mhs_nilai_list', $data);
		$this->load->view('layouts/footer');

	}

	public function results()
	{
		$nilai_id = $this->input->post('nilai_id');
		$status = $this->input->post('statusx');
		$date =  date('Y-m-d');

		$check_status = $this->nilai_model->check_status("tb_nilai",$nilai_id)->result();

		if ($status == "Disetujui"){
			$data 	  = array('status' => 'Disetujui',
							  'tgl_persetujuan' => $date);
			$message  = "Di Setujui !"; 

		} else {
			$data 	  = array('status' => 'Tidak Disetujui',
							  'tgl_persetujuan' => '0000-00-00');
			$message  = "Tidak Disetujui !"; 
		}

		$this->nilai_model->update_nilai('tb_nilai',$nilai_id,$data);

		echo "<script>alert('Data pengguna berhasil $message')</script>";
		echo "<script>window.location='../nilai/mhs_nilai'</script>";
	}
}