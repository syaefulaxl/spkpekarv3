<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');
        $this->load->model('users_model');
        $this->load->model('department_model');
        $this->load->library('form_validation');

        $username = $this->session->userdata('username');
		$username = array('username' => $username );

		$check = $this->auth_model->check("users", $username)->result();

		$access = array('Admin');

		if(!in_array($check[0]->status_login, $access)){
			redirect(base_url("auth/logout"));

		}

		if($check[0]->status == "Tidak Aktif"){
			redirect(base_url("auth/logout"));

		}

    }

	public function index()
	{

		$username = $this->session->userdata('username');

		$where = array('username' => $username);
		$data = $this->auth_model->user_data("users",$where)->result();

		$data['name'] = $data[0]->nama;
		$data['status_login'] = $data[0]->status_login;
		
		$data['data_department'] = $this->department_model->table_data("tb_department")->result();
		$idMax = $this->department_model->data_users();
		$noUrut =(int) substr($idMax[0]->maxID,2,3);
        $noUrut ++;
        $data['newID']="US".sprintf("%03s",$noUrut);

		$this->load->view('layouts/header');
		$this->load->view('layouts/navigation', $data);
		$this->load->view('admin/department', $data);
		$this->load->view('layouts/footer');


			
	}

	public function add()
	{

		$this->form_validation->set_rules("nama_Dept", "Nama Department", "trim|required");
		$this->form_validation->set_rules("kepala_dept", "Kepala Department", "trim|required");

		if ($this->form_validation->run() == true){

			$nama_dept = $this->input->post('nama_dept');
			$kepala_dept = $this->input->post('kepala_dept)');

			$nama_find = array('nama_dept' => $nama_dept);
			$cek_nama = $this->department_model->validate_data("tb_department",$nama_find)->num_rows();

			if ($cek_nama > 0) {
				echo "<script>alert('nidn sudah digunakan !')</script>";
				echo "<script>history.back(1)</script>";

			} else {

			$data 	  = array(
				'nama_dept' => $nama_Dept, 
				'kepala_dept' => $kepala_dept,
				'status' => 'Aktif'); 

			$insert = $this->department_model->insert('tb_department',$data);

			echo "<script>alert('Data Department berhasil di simpan.')</script>";
			echo "<script>window.location='../department'</script>";

			/*if ($cek_nidn > 0) {
				echo "<script>alert('Username(nidn,NIDN,Users) sudah digunakan !')</script>";
				echo "<script>history.back(1)</script>";
				
			} else if ($cek_email > 0) {
				echo "<script>alert('E-mail sudah digunakan !')</script>";
				echo "<script>history.back(1)</script>";
				
			} else {

			$data 	  = array('username' => $nidn, 'nama' => $nama_dsn, 'email' => $email_dsn, 'password' => sha1($nidn), 'status_login' => 'department', 'status' => 'Aktif'); 

			$insert = $this->department_model->insert('users',$data);

			echo "<script>alert('Data Untuk Login department Berhasil Di Simpan.')</script>";
			echo "<script>window.location='../department'</script>";
			}*/
			}

		} else {

			echo "<script>alert('Data Department Gagal Di Simpan.')</script>";
			echo "<script>window.location='../department'</script>";
		
		} 

	}

	public function update()
	{

		$this->form_validation->set_rules("enidn", "nidn", "trim|required");
		$this->form_validation->set_rules("enama_dsn", "Nama department", "trim|required");
		$this->form_validation->set_rules("eprogram", "Program Kelas", "trim|required");
		$this->form_validation->set_rules("esemester", "Semester", "trim|required");
		$this->form_validation->set_rules("eprodi_id", "Program Studi", "trim|required");
		$this->form_validation->set_rules("ekelamin_dsn", "Kelamin department", "trim|required");
		$this->form_validation->set_rules("eemail_dsn", "E-Mail department", "trim|required");
		$this->form_validation->set_rules("etanggal_lahir_dsn", "Tanggal Lahir department", "trim|required");



		if ($this->form_validation->run() == true){

			$nidn = $this->input->post('enidn');
			$nama_dsn = $this->input->post('enama_dsn');
			$program = $this->input->post('eprogram');
			$semester = $this->input->post('esemester');
			$prodi_id = $this->input->post('eprodi_id');
			$kelamin_dsn = $this->input->post('ekelamin_dsn');
			$email_dsn = $this->input->post('eemail_dsn');
			$tanggal_lahir_dsn = $this->input->post('etanggal_lahir_dsn');

			$data 	  = array(
				'nama_dsn' => $nama_dsn,
				'program' => $program,
				'semester' => $semester,
				'prodi_id' => $prodi_id,
				'kelamin_dsn' => $kelamin_dsn, 
				'email_dsn' => $email_dsn, 
				'tanggal_lahir_dsn' => $tanggal_lahir_dsn, 
				'status' => 'Aktif');  

			$insert = $this->department_model->update('tb_department',$nidn,$data);

			echo "<script>alert('Data department Berhasil Di Update.')</script>";
			echo "<script>window.location='../department'</script>";
			
		} else {

			echo "<script>alert('Data department Gagal Di Update.')</script>";
			echo "<script>window.location='../department'</script>";
		
		} 
			
	}

	public function update_status()
	{

		$nidn = $this->input->get('nidn');
		$where_status = array('nidn' => $nidn);

		$status = $this->department_model->check_status("tb_department",$where_status)->result();

		if ($status[0]->status == "Tidak Aktif"){
			$data 	  = array('status' => 'Aktif');
			$message  = "Di Akfikan !"; 

		} else {
			$data 	  = array('status' => 'Tidak Aktif');
			$message  = "Di Nonaktifkan !"; 
		}

		$this->department_model->update_status('tb_department',$nidn,$data);

		echo "<script>alert('Data department berhasil $message')</script>";
		echo "<script>window.location='../department'</script>";
			
	}


	public function delete()
	{

		$nidn = $this->input->get('nidn');
		$check_department = array('nidn' => $nidn);
		$check_data = $this->department_model->check_data("tb_department", $check_department)->num_rows();

		if ($check_data == 1){
			$this->department_model->delete('tb_department',$nidn);
			$this->users_model->delete('users',$nidn);

			echo "<script>alert('Data department berhasil di hapus')</script>";
			echo "<script>alert('Data pengguna department berhasil di hapus')</script>";
			echo "<script>window.location='../department'</script>";

		} else {
			echo "<script>alert('Data department tidak bisa di hapus, karena telah berkaitan dengan data lain !')</script>";
			echo "<script>window.location='../department'</script>";

		}

	}
}
