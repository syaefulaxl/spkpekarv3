<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kriteria extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');
        $this->load->model('users_model');
        $this->load->model('kriteria_model');
        $this->load->library('form_validation');

        $username = $this->session->userdata('username');
		$username = array('username' => $username );

		$check = $this->auth_model->check("users", $username)->result();

		$access = array('Admin');

		if(!in_array($check[0]->status_login, $access)){
			redirect(base_url("auth/logout"));

		}

		if($check[0]->status == "Tidak Aktif"){
			redirect(base_url("auth/logout"));

		}

    }

	public function index()
	{

		$username = $this->session->userdata('username');

		$where = array('username' => $username);
		$data = $this->auth_model->user_data("users",$where)->result();

		$data['name'] = $data[0]->nama;
		$data['status_login'] = $data[0]->status_login;

		$data['data_kriteria'] = $this->kriteria_model->table_data("tb_kriteria")->result();

		$idMax = $this->kriteria_model->data_users();
		$noUrut =(int) substr($idMax[0]->maxID,2,3);
        $noUrut ++;
        $data['newID']="US".sprintf("%03s",$noUrut);

		$this->load->view('layouts/header');
		$this->load->view('layouts/navigation', $data);
		$this->load->view('admin/kriteria', $data);
		$this->load->view('layouts/footer');


			
	}

	public function add()
	{

		$this->form_validation->set_rules("kode_kriter", "Kode Kriteria", "trim|required");
		$this->form_validation->set_rules("nama_kriter", "Nama kriteria", "trim|required");
		$this->form_validation->set_rules("deskripsi", "Deskripsi", "trim");

		if ($this->form_validation->run() == true){

			$kode_kriter = $this->input->post('kode_kriter');
			$nama_kriter = $this->input->post('nama_kriter');
			$deskripsi = $this->input->post('deskripsi');
			
			$kode_kriter_find = array('kode_kriter' => $kode_kriter);
			$cek_kode_kriter = $this->kriteria_model->validate_username("tb_kriteria",$kode_kriter_find)->num_rows();

			if ($cek_kode_kriter > 0) {
				echo "<script>alert('Kode Sudah Digunakan !')</script>";
				echo "<script>history.back(1)</script>";
				
			} else {

			$data 	  = array(
				'kode_kriter' => $kode_kriter, 
				'nama_kriter' => $nama_kriter,
				'deskripsi' => $deskripsi);

			$insert = $this->kriteria_model->insert('tb_kriteria',$data);

			echo "<script>alert('Data kriteria berhasil di simpan.')</script>";
			echo "<script>window.location='../kriteria'</script>";	
			} 
		
		}else {
			echo "<script>alert('Data kriteria Gagal Di Simpan.')</script>";
			echo "<script>window.location='../kriteria'</script>";			
		} 

	}

	public function update()
	{

		$this->form_validation->set_rules("ekode_kriter", "Kode kriteria", "trim|required");
		$this->form_validation->set_rules("enama_kriter", "Nama kriteria", "trim|required");
		$this->form_validation->set_rules("ebobot", "Bobot", "trim|required");
		$this->form_validation->set_rules("edeskripsi", "deskripsi", "trim");

		$kode_kriter = $this->input->post('ekode_kriter');

		if($kode_kriter == "ABSEN"){

			$atribut = "Benefit";
		} else {
			$this->form_validation->set_rules("eatribut", "Atribut", "trim|required");

            $atribut = $this->input->post('eatribut');
		}


		if ($this->form_validation->run() == true){

			$kode_kriter = $this->input->post('ekode_kriter');
			$nama_kriter = $this->input->post('enama_kriter');
			$atribut = $atribut;
			$bobot = $this->input->post('ebobot');
			$deskripsi = $this->input->post('edeskripsi');
			$data 	  = array(
				'nama_kriter' => $nama_kriter,
				'atribut' => $atribut,
				'bobot' => $bobot,
				'deskripsi' => $deskripsi);

			$insert = $this->kriteria_model->update('tb_kriteria',$kode_kriter,$data);

			echo "<script>alert('Data Kriteria Berhasil Di Update.')</script>";
			echo "<script>window.location='../kriteria'</script>";
			
		} else {

			echo "<script>alert('Data Kriteria Gagal Di Update.')</script>";
			echo "<script>window.location='../kriteria'</script>";
		
		} 
			
	}

	public function delete()
	{

		$kode_kriter = $this->input->get('kode_kriter');
		$check_kriteria = array('kode_kriter' => $kode_kriter);
		$check_data = $this->kriteria_model->check_data("tb_kriteria", $check_kriteria)->num_rows();

		if ($check_data == 1){
			$this->kriteria_model->delete('tb_kriteria',$kode_kriter);
			$this->users_model->delete('users',$kode_kriter);

			echo "<script>alert('Data Kriteria berhasil di hapus')</script>";
			echo "<script>window.location='../kriteria'</script>";

		} else {
			echo "<script>alert('Data Kriteria tidak bisa di hapus, karena telah berkaitan dengan data lain !')</script>";
			echo "<script>window.location='../kriteria'</script>";

		}

	}
}
