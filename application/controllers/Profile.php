<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');
        $this->load->model('users_model');
        $this->load->library('form_validation');

        $username = $this->session->userdata('username');
		$username = array('username' => $username );

		$check = $this->auth_model->check("users", $username)->result();

		$access = array('Admin','Mahasiswa','Prodi');

		if(!in_array($check[0]->status_login, $access)){
			redirect(base_url("auth/logout"));

		}

		if($check[0]->status == "Tidak Aktif"){
			redirect(base_url("auth/logout"));

		}

    }

	public function index()
	{
		$username = $this->session->userdata('username');

		$where = array('username' => $username);
		$data = $this->users_model->check_data("users",$where)->result();

		$data['name'] = $data[0]->nama;
		$data['status_login'] = $data[0]->status_login;

		$where = array('username' => $username);
		
		$data['data_users'] = $this->users_model->check_data("users",$where)->result();

		$this->load->view('layouts/header');
		$this->load->view('layouts/navigation', $data);
		$this->load->view('root/profile', $data);
		$this->load->view('layouts/footer');
		
	}

	public function update_akun()
	{

		$this->form_validation->set_rules("username", "Username", "trim|required");
		$this->form_validation->set_rules("nama", "Nama", "trim|required");
		$this->form_validation->set_rules("email", "E-Mail", "trim|required");
		$this->form_validation->set_rules("baru", "Password Baru", "trim|required");
		$this->form_validation->set_rules("lama", "Password Lama", "trim|required");

		if ($this->form_validation->run() == true){

			$username = $this->input->post('username');
			$nama = $this->input->post('nama');
			$baru = $this->input->post('baru');
			$Bpassword = sha1($baru);
			$lama = $this->input->post('lama');
			$Lpassword = sha1($lama);

			$where = array('password' => $Lpassword, 'Username' => $username);

			$data_users = $this->users_model->check_data("users",$where)->num_rows();

			if ($data_users == 0) {
				echo "<script>alert('Password lama anda tidak cocok !')</script>";
				echo "<script>window.location='../profile'</script>";
				
			} else {

			$data 	  = array('nama' => $nama, 'password' => $Bpassword); 

			$insert = $this->users_model->update('users',$username,$data);

			echo "<script>alert('Data pengguna berhasil di update.')</script>";
			echo "<script>window.location='../profile'</script>";
			}

		} else {

			echo "<script>window.location='../profile'</script>";
		
		} 
			
	}
}
