<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
	    $this->load->model('auth_model');
	    $this->load->model('crud_model');
	    $this->load->library('form_validation');

    }

	public function index()
	{
		$this->load->view('auth/login');
	}

	public function forgot()
	{	

		$this->load->view("auth/forgot");
	}

	public function send()
	{	
		$this->form_validation->set_rules("email", "E-mail", "trim|required");

		if ($this->form_validation->run() == true){


			$db = get_instance()->db->conn_id;

			$send = $this->input->post('email');

			$pass = "1A2B4HTjsk5kwhadbwlff"; 
			$panjang = '8'; 
			$len = strlen($pass);
			$start = $len - $panjang; 
			$xx = rand('0',$start);
			$yy = str_shuffle($pass);

			$passwordbaru = substr($yy, $xx, $panjang);

			$email = trim(strip_tags($send));
			$password = mysqli_real_escape_string($db, htmlentities((sha1($passwordbaru))));

			date_default_timezone_set("Asia/Jakarta");
			$date = date("d, M Y");

			$where = array('email' => $email, 'status' => 'Aktif');
			$check_email = $this->auth_model->check("users",$where)->num_rows();

			if($check_email > 0){

			$data = $this->auth_model->user_data("users",$where)->result();

			$kode_pengguna = strip_tags($data[0]->kode_pengguna);
			$username = trim(strip_tags($data[0]->username));
			$nama = strip_tags($data[0]->nama);
			$email = strip_tags($data[0]->email);

			$data_update = array('password' => $password);
			$update = $this->auth_model->update('users',$kode_pengguna,$data_update);


			$title = "Memperbaharui Password";

			$pesan = "Kami Telah Merubah Password dari Nama : ".$nama."
			  Sekarang anda dapat login kembali dengan data ini \n\n
			  Akun Anda :\n
			  Username : ".$username." \n
			  Password : ".$passwordbaru."\n\n
			  \n\n MESSAGE NO-REPLY";

			$header = "From: dalima@dalima.xyz<no-reply@dalima.xyz>";


			// echo "	<center><h3>Contoh E-Mail</h3><hr>
			// <h3>Title :</h3>".$title."<hr>
			// <h3>Header :</h3>".$header."<hr>
			// <h3>Mail :</h3>".$pesan."<hr></center>";
			// echo "<center><br><a href='../auth'>Back</a></center>";

			mail($email, $title, $pesan, $header);
			
			echo "<script>alert('Password baru telah dikirim ke alamat email : ".$email." Mohon cek E-Mail anda..')</script>";
			echo "<script>window.location='../auth'</script>";



			}else{

			echo "<script>alert('E-mail tidak ditemukan atau anda sudah bukan user aktif / sudah di nonaktifkan oleh admin kami.')</script>";
			echo "<script>history.back(1)</script>";
			
			}

		} else {
		    echo "<script>history.back(1)</script>";
		}
	}

	public function login()
	{	
		$this->form_validation->set_rules("username", "Username", "trim|required");
		$this->form_validation->set_rules("password", "Password", "trim|required");

		if ($this->form_validation->run() == true){

			$db = get_instance()->db->conn_id;

			$username = mysqli_real_escape_string($db, $this->input->post('username'));
			$password = mysqli_real_escape_string($db, sha1($this->input->post('password')));

			$where = array('username' => $username, 'password' => $password , 'status' => 'aktif');
			
			$check_validation = $this->crud_model->get_data("users",$where)->num_rows();

				if($check_validation > 0){

				$data = $this->crud_model->get_data("users",$where)->result();

				$data_session = array(
					'username' => $data[0]->username,
					'password' => $data[0]->password,
					'status_login' => $data[0]->status_login
				);

				$this->session->set_userdata($data_session);

				date_default_timezone_set("Asia/Jakarta");
				$date = date('Y-m-d H:i:s');

				echo"<script>alert('Berhasil Masuk Sebagai ".$data[0]->status_login." Selamat Datang.')</script>";

				if ($data[0]->status_login == "Admin") {
					echo"<script>window.location='../dashboard'</script>";	

				} elseif ($data[0]->status_login == "Manajer") {
					echo"<script>window.location='../dashboard'</script>";

				} elseif ($data[0]->status_login == "Direktur"){
					echo"<script>window.location='../dashboard'</script>";
				}
				die();

				}else{
				echo "<script>alert('Anda Gagal Masuk !, Username / Password anda salah, atau anda sudah dinonaktifkan. ')</script>";
				echo "<script>window.location='".base_url()."'</script>";
				die();
			
			}
		}
	}

	public function logout()
	{
		$this->session->unset_userdata("username", "status_login");
		$this->session->sess_destroy();
		
		echo "<script>alert('Terimakasih telah menggunakan aplikasi ini.')</script>";
		echo "<script>window.location='../auth'</script>";
		
	}
}
