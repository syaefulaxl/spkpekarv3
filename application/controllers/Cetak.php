<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');
        $this->load->model('users_model');
        $this->load->model('cetak_model');
        $this->load->library('form_validation');

        $username = $this->session->userdata('username');
        $username = array('username' => $username );

        $check = $this->auth_model->check("users", $username)->result();

        $access = array('Admin','Mahasiswa','Prodi');

        if(!in_array($check[0]->status_login, $access)){
            redirect(base_url("auth/logout"));

        }

        if($check[0]->status == "Tidak Aktif"){
            redirect(base_url("auth/logout"));

        }

    }

    public function index()
    {
        $username = $this->session->userdata('username');

        $krs_id = $this->input->get('krs_id');
        $status = $this->input->get('status');

        $where = array('username' => $username);
        $data = $this->auth_model->user_data("users",$where)->result();

        $data['name'] = $data[0]->nama;
        $data['status_login'] = $data[0]->status_login;

        $data['data_krs'] = $this->cetak_model->cetak_krs($krs_id);

        $this->load->view('layouts/header');
        $this->load->view('layouts/navigation', $data);
        $this->load->view('root/printkrs', $data);
        $this->load->view('layouts/footer');             
    }

    public function pdf_ex()
    {
        $username = $this->session->userdata('username');

        $krs_id = $this->input->get('krs_id');
        $status = $this->input->get('status');

        $where = array('username' => $username);
        $data = $this->auth_model->user_data("users",$where)->result();

        $data['name'] = $data[0]->nama;
        $data['status_login'] = $data[0]->status_login;

        $data['data_krs'] = $this->cetak_model->cetak_krs($krs_id);

        //load the view and saved it into $html variable
        $html=$this->load->view('root/printkrs', $data, true);
 
        //this the the PDF filename that user will get to download
        $pdfFilePath = "Perwalian.pdf";
 
        //load mPDF library
        $this->load->library('m_pdf');
 
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");        
    }

}

