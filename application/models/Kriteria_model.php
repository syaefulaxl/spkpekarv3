<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kriteria_model extends CI_Model {

  public function table_data($table)
   { 
    return $this->db->get($table);

   }     

  public function data_users()
   { 
    return $this->db->query("SELECT max(username) as maxID FROM users")->result();

   }

  public function insert($table, $data)
    {
        $this->db->insert($table, $data);
    
    }

   public function update($table, $kode_kriter, $data)
    {
        $this->db->where('kode_kriter', $kode_kriter);
        $this->db->update($table, $data);
    }

  public function check_data($table, $kode_kriter)
  {   
    return $this->db->get_where($table,$kode_kriter);

  }

  public function delete($table, $kode_kriter)
  {
    $this->db->where('kode_kriter', $kode_kriter);
    $this->db->delete($table);
  }

  public function check_max_k1()
  {
    $this->db->select("max(pd.k1) as max_k1")
             ->from("tb_nilai_detail pd");
    return $this->db->get()->result();
  }

  public function check_max_k2()
  {
    $this->db->select("max(pd.k2) as max_k2")
             ->from("tb_nilai_detail pd");
    return $this->db->get()->result();
  }

  public function check_max_k3()
  {
    $this->db->select("max(pd.k3) as max_k3")
             ->from("tb_nilai_detail pd");
    return $this->db->get()->result();
  }

  public function check_max_k4()
  {
    $this->db->select("max(pd.k4) as max_k4")
             ->from("tb_nilai_detail pd");
    return $this->db->get()->result();
  }

  public function check_max_k5()
  {
    $this->db->select("max(pd.k5) as max_k5")
             ->from("tb_nilai_detail pd");
    return $this->db->get()->result();
  }

  public function check_max_absen()
  {
    $this->db->select("max(pd.absen) as max_absen")
             ->from("tb_nilai_detail pd");
    return $this->db->get()->result();
  }
  


  public function check_min_k1()
  {
    $this->db->select("min(pd.k1) as min_k1")
             ->from("tb_nilai_detail pd");
    return $this->db->get()->result();
  }

  public function check_min_k2()
  {
    $this->db->select("min(pd.k2) as min_k2")
             ->from("tb_nilai_detail pd");
    return $this->db->get()->result();
  }

  public function check_min_k3()
  {
    $this->db->select("min(pd.k3) as min_k3")
             ->from("tb_nilai_detail pd");
    return $this->db->get()->result();
  }

  public function check_min_k4()
  {
    $this->db->select("min(pd.k4) as min_k4")
             ->from("tb_nilai_detail pd");
    return $this->db->get()->result();
  }

  public function check_min_k5()
  {
    $this->db->select("min(pd.k5) as min_k5")
             ->from("tb_nilai_detail pd");
    return $this->db->get()->result();
  }

  public function check_min_absen()
  {
    $this->db->select("min(pd.absen) as min_absen")
             ->from("tb_nilai_detail pd");
    return $this->db->get()->result();
  }
}