<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Karyawan_model extends CI_Model {

  public function table_data($table)
   { 
    return $this->db->get($table);

   }     

  public function data_users()
   { 
    return $this->db->query("SELECT max(username) as maxID FROM users")->result();

   }

  public function insert($table, $data)
    {
        $this->db->insert($table, $data);
    
    }

  public function validate_username($table, $username)
   {   
       return $this->db->get_where($table,$username);

   }

   public function validate_email($table, $email)
   {   
       return $this->db->get_where($table,$email);

   }   

   public function update_status($table, $emp, $data)
   {
    $this->db->where('emp', $emp);
    $this->db->update($table, $data);
   
   }

   public function update($table, $emp, $data)
    {
        $this->db->where('emp', $emp);
        $this->db->update($table, $data);
    }

  public function check_status($table, $emp)
  {   
    return $this->db->get_where($table,$emp);

  }

  public function check_data($table, $emp)
  {   
    return $this->db->get_where($table,$emp);

  }

  public function karyawan_depart()
  {
    $this->db->select("kary.*, dept.nama_dept")
             ->from("tb_karyawan kary")
             ->join("tb_department dept", "kary.dept_id = dept.dept_id");
    return $this->db->get()->result();
  } 

  public function delete($table, $emp)
  {
    $this->db->where('emp', $emp);
    $this->db->delete($table);
  }

  public function karyawan_emp()
  {   
        return $this->db->query("SELECT max(emp) as maxID FROM tb_karyawan WHERE emp LIKE 'EMP%'")->result();

  }
}