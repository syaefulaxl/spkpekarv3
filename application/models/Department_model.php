<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department_model extends CI_Model {

  public function table_data($table)
   { 
    return $this->db->get($table);

   }     

  public function data_users()
   { 
    return $this->db->query("SELECT max(username) as maxID FROM users")->result();

   }

  public function insert($table, $data)
    {
        $this->db->insert($table, $data);
    
    }

  public function validate_username($table, $username)
   {   
       return $this->db->get_where($table,$username);

   }

   public function validate_data($table, $nama)
   {   
       return $this->db->get_where($table,$nama);

   }   

   public function update_status($table, $nidn, $data)
   {
    $this->db->where('nidn', $nidn);
    $this->db->update($table, $data);
   
   }

   public function update($table, $nidn, $data)
    {
        $this->db->where('nidn', $nidn);
        $this->db->update($table, $data);
    }

  public function check_status($table, $nidn)
  {   
    return $this->db->get_where($table,$nidn);

  }

  public function check_data($table, $nidn)
  {   
    return $this->db->get_where($table,$nidn);

  }

  public function delete($table, $nidn)
  {
    $this->db->where('nidn', $nidn);
    $this->db->delete($table);
  }
}