<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {

	public function user_data($table)
	{	
		return $this->db->get($table);

	}

	public function users_username()
	{	
		return $this->db->query("SELECT max(username) as maxID FROM users")->result();

	}

    public function get_data_from_username($table, $username)
    {   
        return $this->db->get_where($table,$username);

    }

    public function validate_username($table, $username)
    {   
        return $this->db->get_where($table,$username);

    }

    public function validate_email($table, $email)
    {   
        return $this->db->get_where($table,$email);

    }

    public function check_data($table, $kode_pengguna)
    {   
        return $this->db->get_where($table,$kode_pengguna);

    }

    public function check_status($table, $kode_pengguna)
    {   
        return $this->db->get_where($table,$kode_pengguna);

    }

	public function insert($table, $data)
    {
        $this->db->insert($table, $data);
    }

    // update data
    public function update($table, $kode_pengguna, $data)
    {
        $this->db->where('username', $kode_pengguna);
        $this->db->update($table, $data);
    }

    // delete data
    public function delete($table, $kode_pengguna)
    {
        $this->db->where('username', $kode_pengguna);
        $this->db->delete($table);
    }


}