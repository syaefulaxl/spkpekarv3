<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_model extends CI_Model {

	public function get_data($table, $where)
	{	
		return $this->db->get_where($table,$where);
    
	}

  public function insert($table, $data)
  {
    $this->db->insert($table, $data);

  }

  public function update($table, $field, $id, $data)
  {
    $this->db->where($field, $id);
    $this->db->update($table, $data);

  }

  public function delete($table, $field, $id)
  {
    $this->db->where($field, $id);
    $this->db->delete($table);

  }
	
}