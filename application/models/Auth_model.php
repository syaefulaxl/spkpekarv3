<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model {

	public function validate($table, $where)
	{	
		return $this->db->get_where($table,$where);

	}
	public function user_data($table, $where)
	{	
		return $this->db->get_where($table,$where);

	}
	public function check($table, $where)
	{	
		return $this->db->get_where($table,$where);

	}
	public function update($table, $username, $data)
    {
        $this->db->where('username', $username);
        $this->db->update($table, $data);
    }

}