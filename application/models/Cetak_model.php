
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cetak_model extends CI_Model {

	public function cetak_krs($krs_id)
	{	

		$this->db->select("pro.nama_prodi, mhs.nama_mhs, mhs.nim, mhs.semester, krsd.kode_mk, mk.nama_mk, mk.sks, krs.*")
                 ->from("tb_krs krs, tb_mahasiswa mhs")
                 ->join("tb_krs_detail krsd", "krs.krs_id = krsd.krs_id")
                 ->join("tb_matakuliah mk", "mk.kode_mk = krsd.kode_mk")
                 ->join("tb_prodi pro", "mhs.prodi_id = pro.prodi_id")
                 ->where("krs.krs_id", $krs_id)
                 ->order_by('mk.nama_mk', 'ASC');
        return $this->db->get()->result();

	}

    public function project_data2($kode_proyek)
    {   

        $this->db->select("p.*, c.instansi")
                 ->from("project p")
                 ->join("client c", "p.kode_klien = c.kode_klien")
                 ->where("p.kode_proyek", $kode_proyek)
                 ->order_by('tanggal', 'DESC');
        return $this->db->get()->result();

    }

    public function sum_ba($kode_proyek)
    {   

        $this->db->select("sum(e.bobot_aktual) as jmlBA")
                 ->from("evm e")
                 ->where("e.kode_proyek", $kode_proyek);
        return $this->db->get()->result();

    }

}