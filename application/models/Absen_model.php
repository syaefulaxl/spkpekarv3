<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Absen_model extends CI_Model {

  public function table_data($table)
   { 
    return $this->db->get($table);

   }     

  public function data_users()
   { 
    return $this->db->query("SELECT max(username) as maxID FROM users")->result();

   }

  public function insert($table, $data)
    {
        $this->db->insert($table, $data);
    
    }

  public function validate_username($table, $username)
   {   
       return $this->db->get_where($table,$username);

   }

   public function validate_email($table, $email)
   {   
       return $this->db->get_where($table,$email);

   }   

   public function update_status($table, $absen_id, $data)
   {
    $this->db->where('absen_id', $absen_id);
    $this->db->update($table, $data);
   
   }

   public function update($table, $absen_id, $data)
    {
        $this->db->where('absen_id', $absen_id);
        $this->db->update($table, $data);
    }

  public function check_status($table, $absen_id)
  {   
    return $this->db->get_where($table,$absen_id);

  }

  public function check_data($table, $absen_id)
  {   
    return $this->db->get_where($table,$absen_id);

  }

  public function absen()
  {
    $this->db->select("mk.nama_mk, dsn.nama_dsn, abs.*")
             ->from("tb_absen abs")
             ->join("tb_matakuliah mk", "mk.kode_mk = abs.kode_mk")
             ->join("tb_dosen dsn", "dsn.nidn = abs.nidn")
             ->order_by('abs.hari', 'ASC');
    return $this->db->get()->result();
  } 

  public function delete($table, $absen_id)
  {
    $this->db->where('absen_id', $absen_id);
    $this->db->delete($table);
  }
}