<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nilai_model extends CI_Model {

  public function table_data($table)
   { 
    return $this->db->get($table);

   }     

  public function data_users()
   { 
    return $this->db->query("SELECT max(username) as maxID FROM users")->result();

   }

  public function insert($table, $data)
    {
        $this->db->insert($table, $data);
    
    }

  public function validate_krs($table, $username)
   {   
       return $this->db->get_where($table,$username);

   }

   public function validate_email($table, $email)
   {   
       return $this->db->get_where($table,$email);

   }   

   public function update_status($table, $emp, $data)
   {
    $this->db->where('emp', $emp);
    $this->db->update($table, $data);
   
   }

   public function update($table, $emp, $data)
    {
        $this->db->where('emp', $emp);
        $this->db->update($table, $data);
    }

   public function update_krs($table, $id, $data)
    {
        $this->db->where('krs_id', $id);
        $this->db->update($table, $data);
    }

  public function update_penilaian($table, $id, $data)
    {
        $this->db->where('nilai_id', $id);
        $this->db->update($table, $data);
    }

  public function check_status($table, $krs_id)
  {   
    return $this->db->get_where($table,$krs_id);

  }

  public function check_data($table, $emp)
  {   
    return $this->db->get_where($table,$emp);

  }

  public function delete($table, $emp)
  {
    $this->db->where('emp', $emp);
    $this->db->delete($table);
  }

  public function delete_penilaian($table, $nilai_id)
  {
    $this->db->where('nilai_id', $nilai_id);
    $this->db->delete($table);
  }

  public function get_kary_nilai()
  {
    $this->db->select("p.*, pd.*, k.nama_kary")
             ->from("tb_nilai p")
             ->join("tb_karyawan k", "p.emp = k.emp")
             ->join("tb_nilai_detail pd", "p.nilai_id = pd.nilai_id")
             ->order_by('p.emp', 'ASC');
    return $this->db->get()->result();
  }

  public function get_kary_nilai_2()
  {
    $this->db->select("k.nama_kary")
             ->from("tb_karyawan k")
             ->order_by('k.emp', 'ASC');
    return $this->db->get()->result();
  }

  public function get_emp_by_name($nama_kary)
  {
    $this->db->select("k.emp")
             ->from("tb_karyawan k");
    $this->db->where("k.nama_kary" , $nama_kary);
    return $this->db->get()->result();
  }

  public function get_laporan_nilai()
  {
    $this->db->select("p.*, pd.*, k.nama_kary")
             ->from("tb_nilai p")
             ->join("tb_karyawan k", "p.emp = k.emp")
             ->join("tb_nilai_detail pd", "p.nilai_id = pd.nilai_id");
    $this->db->where("pd.total != ''");
    return $this->db->get()->result();
  }

  public function nilai_id_code($nilai_id) 
  {   
    return $this->db->query("SELECT max(nilai_id) as maxID 
    FROM tb_nilai WHERE nilai_id LIKE '".$nilai_id."%'")->result();
  }
}